use image::io::Reader as ImageReader;
use std::fs::File;
use std::io::{self, Cursor, Write};
use std::error::Error;

fn d_b64(base64: &str) -> Result<Vec<u8>, Box<dyn Error>> {
    // Base64 character set
    let base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    let mut base64_map = [0u8; 256];
    for (i, &c) in base64_chars.as_bytes().iter().enumerate() {
        base64_map[c as usize] = i as u8;
    }

    let base64 = base64.trim_end_matches('=');
    let mut buffer = Vec::with_capacity(base64.len() * 3 / 4);

    let mut bits = 0u32;
    let mut bits_count = 0;
    for &byte in base64.as_bytes() {
        bits = (bits << 6) | base64_map[byte as usize] as u32;
        bits_count += 6;

        if bits_count >= 8 {
            bits_count -= 8;
            buffer.push((bits >> bits_count) as u8);
            bits &= (1 << bits_count) - 1;
        }
    }

    Ok(buffer)
}

pub fn save_image_data(data_url: &str, path: &str) -> Result<(), Box<dyn Error>> {
    let data = data_url.split(",").nth(1).unwrap();
    let bytes = d_b64(data)?;
    // let mut reader = ImageReader::new(Cursor::new(bytes));
    // let image = reader.with_guessed_format()?.decode()?;
    // image.save(path)?;
    let mut file = File::create(path)?;
        file.write_all(&bytes)?;
    Ok(())
}