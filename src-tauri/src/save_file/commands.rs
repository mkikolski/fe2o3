#[tauri::command]
pub async fn save_image(handle: tauri::AppHandle, path: &str, data_url: &str) -> Result<(), String> {
  crate::save_file::save_image_data(data_url, path).map_err(|e| e.to_string())?;
  Ok(())
}