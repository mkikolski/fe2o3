use lazy_static::lazy_static;
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use serde::{Deserialize, Serialize};
use purr::graph::Atom;
use purr::feature::{AtomKind, BondKind, Aromatic, BracketSymbol, Element, Configuration, Aliphatic};
use tauri::{AppHandle, Manager};

/// Represents the descriptor for an atom.
///
/// # Fields
///
/// * `name` - The name of the atom.
/// * `mass` - The mass of the atom.
/// * `radius` - The radius of the atom.
///
/// # Remarks
///
/// This struct is used to store the descriptor for an atom, including its name, mass, and radius. It is used in the `load_atom_descriptors` function to create a map of atom descriptors from a JSON file. The struct is serializable and deserializable, which allows it to be easily written to and read from a file.
#[derive(Serialize, Deserialize, Debug)]
pub struct AtomDescriptor {
    pub name: String,
    pub mass: f32,
    pub radius: f32,
}

/// A static `HashMap` that maps atom names to their descriptors.
///
/// # Remarks
///
/// This `HashMap` is initialized once and can be used throughout the program to look up the descriptor for a given atom by its name. The descriptors are loaded from a JSON file at a specified path. The file is expected to contain a JSON object where each key is an atom name and each value is an `AtomDescriptor` object.
///
/// If the file cannot be opened or the JSON cannot be parsed, the program will panic.
lazy_static! {
    pub static ref ATOMS_DATA: HashMap<String, AtomDescriptor> = HashMap::from([
        ("H".to_string(), AtomDescriptor { name: "Hydrogen".to_string(), mass: 1.008, radius: 53.0 }),
        ("He".to_string(), AtomDescriptor { name: "Helium".to_string(), mass: 4.0026, radius: 31.0 }),
        ("Li".to_string(), AtomDescriptor { name: "Lithium".to_string(), mass: 6.94, radius: 167.0 }),
        ("Be".to_string(), AtomDescriptor { name: "Beryllium".to_string(), mass: 9.0122, radius: 112.0 }),
        ("B".to_string(), AtomDescriptor { name: "Boron".to_string(), mass: 10.81, radius: 87.0 }),
        ("C".to_string(), AtomDescriptor { name: "Carbon".to_string(), mass: 12.011, radius: 67.0 }),
        ("N".to_string(), AtomDescriptor { name: "Nitrogen".to_string(), mass: 14.007, radius: 56.0 }),
        ("O".to_string(), AtomDescriptor { name: "Oxygen".to_string(), mass: 15.999, radius: 48.0 }),
        ("F".to_string(), AtomDescriptor { name: "Fluorine".to_string(), mass: 18.998, radius: 42.0 }),
        ("Ne".to_string(), AtomDescriptor { name: "Neon".to_string(), mass: 20.18, radius: 38.0 }),
        ("Na".to_string(), AtomDescriptor { name: "Sodium".to_string(), mass: 22.99, radius: 190.0 }),
        ("Mg".to_string(), AtomDescriptor { name: "Magnesium".to_string(), mass: 24.305, radius: 145.0 }),
        ("Al".to_string(), AtomDescriptor { name: "Aluminum".to_string(), mass: 26.982, radius: 118.0 }),
        ("Si".to_string(), AtomDescriptor { name: "Silicon".to_string(), mass: 28.085, radius: 111.0 }),
        ("P".to_string(), AtomDescriptor { name: "Phosphorus".to_string(), mass: 30.974, radius: 98.0 }),
        ("S".to_string(), AtomDescriptor { name: "Sulfur".to_string(), mass: 32.06, radius: 88.0 }),
        ("Cl".to_string(), AtomDescriptor { name: "Chlorine".to_string(), mass: 35.45, radius: 79.0 }),
        ("Ar".to_string(), AtomDescriptor { name: "Argon".to_string(), mass: 39.948, radius: 71.0 }),
        ("K".to_string(), AtomDescriptor { name: "Potassium".to_string(), mass: 39.098, radius: 243.0 }),
        ("Ca".to_string(), AtomDescriptor { name: "Calcium".to_string(), mass: 40.078, radius: 194.0 }),
        ("Sc".to_string(), AtomDescriptor { name: "Scandium".to_string(), mass: 44.956, radius: 184.0 }),
        ("Ti".to_string(), AtomDescriptor { name: "Titanium".to_string(), mass: 47.867, radius: 176.0 }),
        ("V".to_string(), AtomDescriptor { name: "Vanadium".to_string(), mass: 50.942, radius: 171.0 }),
        ("Cr".to_string(), AtomDescriptor { name: "Chromium".to_string(), mass: 51.996, radius: 166.0 }),
        ("Mn".to_string(), AtomDescriptor { name: "Manganese".to_string(), mass: 54.938, radius: 161.0 }),
        ("Fe".to_string(), AtomDescriptor { name: "Iron".to_string(), mass: 55.845, radius: 156.0 }),
        ("Co".to_string(), AtomDescriptor { name: "Cobalt".to_string(), mass: 58.933, radius: 152.0 }),
        ("Ni".to_string(), AtomDescriptor { name: "Nickel".to_string(), mass: 58.693, radius: 149.0 }),
        ("Cu".to_string(), AtomDescriptor { name: "Copper".to_string(), mass: 63.546, radius: 145.0 }),
        ("Zn".to_string(), AtomDescriptor { name: "Zinc".to_string(), mass: 65.38, radius: 142.0 }),
        ("Ga".to_string(), AtomDescriptor { name: "Gallium".to_string(), mass: 69.723, radius: 136.0 }),
        ("Ge".to_string(), AtomDescriptor { name: "Germanium".to_string(), mass: 72.63, radius: 125.0 }),
        ("As".to_string(), AtomDescriptor { name: "Arsenic".to_string(), mass: 74.922, radius: 114.0 }),
        ("Se".to_string(), AtomDescriptor { name: "Selenium".to_string(), mass: 78.971, radius: 103.0 }),
        ("Br".to_string(), AtomDescriptor { name: "Bromine".to_string(), mass: 79.904, radius: 94.0 }),
        ("Kr".to_string(), AtomDescriptor { name: "Krypton".to_string(), mass: 83.798, radius: 88.0 }),
        ("Rb".to_string(), AtomDescriptor { name: "Rubidium".to_string(), mass: 85.468, radius: 265.0 }),
        ("Sr".to_string(), AtomDescriptor { name: "Strontium".to_string(), mass: 87.62, radius: 219.0 }),
        ("Y".to_string(), AtomDescriptor { name: "Yttrium".to_string(), mass: 88.906, radius: 212.0 }),
        ("Zr".to_string(), AtomDescriptor { name: "Zirconium".to_string(), mass: 91.224, radius: 206.0 }),
        ("Nb".to_string(), AtomDescriptor { name: "Niobium".to_string(), mass: 92.906, radius: 198.0 }),
        ("Mo".to_string(), AtomDescriptor { name: "Molybdenum".to_string(), mass: 95.95, radius: 190.0 }),
        ("Tc".to_string(), AtomDescriptor { name: "Technetium".to_string(), mass: 98.0, radius: 183.0 }),
        ("Ru".to_string(), AtomDescriptor { name: "Ruthenium".to_string(), mass: 101.07, radius: 178.0 }),
        ("Rh".to_string(), AtomDescriptor { name: "Rhodium".to_string(), mass: 102.91, radius: 173.0 }),
        ("Pd".to_string(), AtomDescriptor { name: "Palladium".to_string(), mass: 106.42, radius: 169.0 }),
        ("Ag".to_string(), AtomDescriptor { name: "Silver".to_string(), mass: 107.87, radius: 165.0 }),
        ("Cd".to_string(), AtomDescriptor { name: "Cadmium".to_string(), mass: 112.41, radius: 161.0 }),
        ("In".to_string(), AtomDescriptor { name: "Indium".to_string(), mass: 114.82, radius: 156.0 }),
        ("Sn".to_string(), AtomDescriptor { name: "Tin".to_string(), mass: 118.71, radius: 145.0 }),
        ("Sb".to_string(), AtomDescriptor { name: "Antimony".to_string(), mass: 121.76, radius: 133.0 }),
        ("Te".to_string(), AtomDescriptor { name: "Tellurium".to_string(), mass: 127.6, radius: 123.0 }),
        ("I".to_string(), AtomDescriptor { name: "Iodine".to_string(), mass: 126.9, radius: 115.0 }),
        ("Xe".to_string(), AtomDescriptor { name: "Xenon".to_string(), mass: 131.29, radius: 108.0 }),
        ("Cs".to_string(), AtomDescriptor { name: "Cesium".to_string(), mass: 132.91, radius: 298.0 }),
        ("Ba".to_string(), AtomDescriptor { name: "Barium".to_string(), mass: 137.33, radius: 253.0 }),
        ("La".to_string(), AtomDescriptor { name: "Lanthanum".to_string(), mass: 138.91, radius: 195.0 }),
        ("Ce".to_string(), AtomDescriptor { name: "Cerium".to_string(), mass: 140.12, radius: 185.0 }),
        ("Pr".to_string(), AtomDescriptor { name: "Praseodymium".to_string(), mass: 140.91, radius: 185.0 }),
        ("Nd".to_string(), AtomDescriptor { name: "Neodymium".to_string(), mass: 144.24, radius: 185.0 }),
        ("Pm".to_string(), AtomDescriptor { name: "Promethium".to_string(), mass: 145.0, radius: 185.0 }),
        ("Sm".to_string(), AtomDescriptor { name: "Samarium".to_string(), mass: 150.36, radius: 185.0 }),
        ("Eu".to_string(), AtomDescriptor { name: "Europium".to_string(), mass: 151.96, radius: 185.0 }),
        ("Gd".to_string(), AtomDescriptor { name: "Gadolinium".to_string(), mass: 157.25, radius: 180.0 }),
        ("Tb".to_string(), AtomDescriptor { name: "Terbium".to_string(), mass: 158.93, radius: 175.0 }),
        ("Dy".to_string(), AtomDescriptor { name: "Dysprosium".to_string(), mass: 162.5, radius: 175.0 }),
        ("Ho".to_string(), AtomDescriptor { name: "Holmium".to_string(), mass: 164.93, radius: 175.0 }),
        ("Er".to_string(), AtomDescriptor { name: "Erbium".to_string(), mass: 167.26, radius: 175.0 }),
        ("Tm".to_string(), AtomDescriptor { name: "Thulium".to_string(), mass: 168.93, radius: 175.0 }),
        ("Yb".to_string(), AtomDescriptor { name: "Ytterbium".to_string(), mass: 173.04, radius: 175.0 }),
        ("Lu".to_string(), AtomDescriptor { name: "Lutetium".to_string(), mass: 174.97, radius: 175.0 }),
        ("Hf".to_string(), AtomDescriptor { name: "Hafnium".to_string(), mass: 178.49, radius: 175.0 }),
        ("Ta".to_string(), AtomDescriptor { name: "Tantalum".to_string(), mass: 180.95, radius: 175.0 }),
        ("W".to_string(), AtomDescriptor { name: "Tungsten".to_string(), mass: 183.84, radius: 175.0 }),
        ("Re".to_string(), AtomDescriptor { name: "Rhenium".to_string(), mass: 186.21, radius: 175.0 }),
        ("Os".to_string(), AtomDescriptor { name: "Osmium".to_string(), mass: 190.23, radius: 175.0 }),
        ("Ir".to_string(), AtomDescriptor { name: "Iridium".to_string(), mass: 192.22, radius: 175.0 }),
        ("Pt".to_string(), AtomDescriptor { name: "Platinum".to_string(), mass: 195.08, radius: 175.0 }),
        ("Au".to_string(), AtomDescriptor { name: "Gold".to_string(), mass: 196.97, radius: 175.0 }),
        ("Hg".to_string(), AtomDescriptor { name: "Mercury".to_string(), mass: 200.59, radius: 175.0 }),
        ("Tl".to_string(), AtomDescriptor { name: "Thallium".to_string(), mass: 204.38, radius: 175.0 }),
        ("Pb".to_string(), AtomDescriptor { name: "Lead".to_string(), mass: 207.2, radius: 175.0 }),
        ("Bi".to_string(), AtomDescriptor { name: "Bismuth".to_string(), mass: 208.98, radius: 175.0 }),
        ("Po".to_string(), AtomDescriptor { name: "Polonium".to_string(), mass: 209.0, radius: 175.0 }),
        ("At".to_string(), AtomDescriptor { name: "Astatine".to_string(), mass: 210.0, radius: 175.0 }),
        ("Rn".to_string(), AtomDescriptor { name: "Radon".to_string(), mass: 222.0, radius: 175.0 }),
        ("Fr".to_string(), AtomDescriptor { name: "Francium".to_string(), mass: 223.0, radius: 175.0 }),
        ("Ra".to_string(), AtomDescriptor { name: "Radium".to_string(), mass: 226.0, radius: 175.0 }),
        ("Ac".to_string(), AtomDescriptor { name: "Actinium".to_string(), mass: 227.0, radius: 175.0 }),
        ("Th".to_string(), AtomDescriptor { name: "Thorium".to_string(), mass: 232.04, radius: 175.0 }),
        ("Pa".to_string(), AtomDescriptor { name: "Protactinium".to_string(), mass: 231.04, radius: 175.0 }),
        ("U".to_string(), AtomDescriptor { name: "Uranium".to_string(), mass: 238.03, radius: 175.0 }),
        ("Np".to_string(), AtomDescriptor { name: "Neptunium".to_string(), mass: 237.0, radius: 175.0 }),
        ("Pu".to_string(), AtomDescriptor { name: "Plutonium".to_string(), mass: 244.0, radius: 175.0 }),
        ("Am".to_string(), AtomDescriptor { name: "Americium".to_string(), mass: 243.0, radius: 175.0 }),
        ("Cm".to_string(), AtomDescriptor { name: "Curium".to_string(), mass: 247.0, radius: 175.0 }),
        ("Bk".to_string(), AtomDescriptor { name: "Berkelium".to_string(), mass: 247.0, radius: 175.0 }),
        ("Cf".to_string(), AtomDescriptor { name: "Californium".to_string(), mass: 251.0, radius: 175.0 }),
        ("Es".to_string(), AtomDescriptor { name: "Einsteinium".to_string(), mass: 252.0, radius: 175.0 }),
        ("Fm".to_string(), AtomDescriptor { name: "Fermium".to_string(), mass: 257.0, radius: 175.0 }),
        ("Md".to_string(), AtomDescriptor { name: "Mendelevium".to_string(), mass: 258.0, radius: 175.0 }),
        ("No".to_string(), AtomDescriptor { name: "Nobelium".to_string(), mass: 259.0, radius: 175.0 }),
        ("Lr".to_string(), AtomDescriptor { name: "Lawrencium".to_string(), mass: 262.0, radius: 175.0 }),
        ("Rf".to_string(), AtomDescriptor { name: "Rutherfordium".to_string(), mass: 267.0, radius: 175.0 }),
        ("Db".to_string(), AtomDescriptor { name: "Dubnium".to_string(), mass: 268.0, radius: 175.0 }),
        ("Sg".to_string(), AtomDescriptor { name: "Seaborgium".to_string(), mass: 271.0, radius: 175.0 }),
        ("Bh".to_string(), AtomDescriptor { name: "Bohrium".to_string(), mass: 270.0, radius: 175.0 }),
        ("Hs".to_string(), AtomDescriptor { name: "Hassium".to_string(), mass: 277.0, radius: 175.0 }),
        ("Mt".to_string(), AtomDescriptor { name: "Meitnerium".to_string(), mass: 276.0, radius: 175.0 }),
        ("Ds".to_string(), AtomDescriptor { name: "Darmstadtium".to_string(), mass: 281.0, radius: 175.0 }),
        ("Rg".to_string(), AtomDescriptor { name: "Roentgenium".to_string(), mass: 280.0, radius: 175.0 }),
        ("Cn".to_string(), AtomDescriptor { name: "Copernicium".to_string(), mass: 285.0, radius: 175.0 }),
        ("Nh".to_string(), AtomDescriptor { name: "Nihonium".to_string(), mass: 284.0, radius: 175.0 }),
        ("Fl".to_string(), AtomDescriptor { name: "Flerovium".to_string(), mass: 289.0, radius: 175.0 }),
        ("Mc".to_string(), AtomDescriptor { name: "Moscovium".to_string(), mass: 288.0, radius: 175.0 }),
        ("Lv".to_string(), AtomDescriptor { name: "Livermorium".to_string(), mass: 293.0, radius: 175.0 }),
        ("Ts".to_string(), AtomDescriptor { name: "Tennessine".to_string(), mass: 294.0, radius: 175.0 }),
        ("Og".to_string(), AtomDescriptor { name: "Oganesson".to_string(), mass: 294.0, radius: 175.0 }),
    ]);
}

/// Loads color settings from a JSON file and returns them as a `HashMap`.
///
/// # Returns
///
/// A `HashMap` where each key is a string representing an atom name and each value is a string representing the color of the atom in hexadecimal format (e.g., "#FFFFFF" for white).
///
/// # Panics
///
/// This function will panic if it fails to open the file or parse the JSON.
///
/// # Remarks
///
/// This function is used to load color settings for atoms from a JSON file. The file is expected to contain a JSON object where each key is an atom name and each value is a color in hexadecimal format. The function uses the `serde_json` crate to parse the JSON into a `HashMap`.
pub fn load_color_settings(handle: tauri::AppHandle) -> HashMap<String, String> {
    let resource_path = handle.path_resolver()
        .resolve_resource("data/color_settings.json")
        .expect("failed to resolve resource");

    let file = File::open(&resource_path).expect("Failed to open file");
    let reader = BufReader::new(file);

    let map: HashMap<String, String> = serde_json::from_reader(reader).expect("Failed to parse JSON");
    map
}

/// Returns the radius of a given atom, optionally scaled by a given factor.
///
/// # Arguments
///
/// * `atom` - A reference to an `Atom` object.
/// * `scale` - An optional `f32` value representing the scale factor. If `None`, the original radius is returned.
///
/// # Returns
///
/// A `f32` value representing the radius of the atom, scaled by the given factor if provided.
///
/// # Panics
///
/// This function will panic if the atom's symbol is not found in the `ATOMS_DATA` map.
///
/// # Remarks
///
/// This function is used to get the radius of an atom for use in generating a 3D model of a molecule. The radius can be scaled by a factor to adjust the size of the atom in the model.
pub fn get_atom_radius(atom: &Atom, scale: Option<f32>) -> f32 {
    let symbol = get_atom_symbol(atom);
    if scale.is_some() {
        return scale.unwrap() * ATOMS_DATA.get(&symbol).unwrap().radius;
    }
    ATOMS_DATA.get(&symbol).unwrap().radius
}

/// Returns the symbol of a given atom as the purr::graph::AtomKind is not directly serializable.
///
/// # Arguments
///
/// * `atom` - A reference to an `Atom` object.
///
/// # Returns
///
/// A `String` representing the symbol of the atom. If the atom is a bracket atom, the symbol is extracted from the `Element` variant of the `AtomKind` enum. Otherwise, the symbol is the string representation of the `AtomKind` enum variant, with any quotation marks removed and converted to uppercase.
///
/// # Panics
///
/// This function will panic if the atom is a bracket atom and the `Element` variant or the closing parenthesis is not found in the string representation of the atom.
pub fn get_atom_symbol(atom: &Atom) -> String {
    let mut strdata = format!("{:?}", atom);
    if strdata.contains("Bracket") {
        let query = "Element(";
        let startidx = strdata.find(query).unwrap();
        strdata = strdata[(startidx + query.len())..].to_string();
        let endidx = strdata.find(")").unwrap();
        strdata = strdata[..endidx].to_string();
        return strdata;
    }
    atom.kind.to_string().replace("\"", "").to_uppercase()
}

#[cfg(test)]
mod tests {
    use purr::feature::{AtomKind, Element};

    use super::*;

    #[test]
    fn test_get_atom_radius() {
        let atom = Atom::new(AtomKind::Aliphatic(purr::feature::Aliphatic::C));
        let radius = get_atom_radius(&atom, None);
        assert_eq!(radius, 67.0);

        let radius = get_atom_radius(&atom, Some(2.0));
        assert_eq!(radius, 134.0);
    }

    
    #[test]
    fn test_get_atom_symbol() {
        let atom = Atom::new(AtomKind::Aliphatic(purr::feature::Aliphatic::C));
        let symbol = get_atom_symbol(&atom);
        assert_eq!(symbol, "C");

        let atom = Atom::new(AtomKind::Bracket{
            symbol: BracketSymbol::Element((Element::Cl)),
            hcount: None,
            charge: None,
            isotope: None,
            configuration: None,
            map: None
        });
        let symbol = get_atom_symbol(&atom);
        assert_eq!(symbol, "Cl");
    }

    // #[test]
    // fn test_load_color_settings() {
    //     let color_settings = load_color_settings();
    //     assert_eq!(color_settings.get("C").unwrap(), "#252525");
    //     assert_eq!(color_settings.get("Cl").unwrap(), "#008080");
    // }
}

