use purr::{graph::{Atom, Builder}, read::{read, Error}};
use nalgebra::{DMatrix, SymmetricEigen};
use tauri::api::process::{Command, CommandEvent};
use std::{borrow::{Borrow, BorrowMut}, f64, future::IntoFuture};
use purr::feature::BondKind;

use crate::parse_mol2;
use crate::add_implicit_hydrogens;

use super::get_atom_symbol;

pub async fn run_openbabel(smiles: &str) -> (Vec<Atom>, Vec<Vec<f64>>, Vec<BondData>) {
    let (mut rx, mut child) = Command::new_sidecar("obabel")
        .expect("failed to register a command")
        .args([&("-:".to_owned() + smiles), "-ismi", "-omol2", "-O", "/dev/stdout", "--gen3d"])
        .spawn()
        .expect("failed to spawn sidecar");

    tauri::async_runtime::spawn(async move {
        let mut lines: Vec<String> = vec![];
        while let Some(event) = rx.recv().await {
            match event {
                CommandEvent::Stdout(event) => {
                    lines.push(event.clone());
                }
                CommandEvent::Error(event)=> {
                    println!("error: {:?}", event);
                }
                _ => {
                    println!("Co xD");
                }
            }
        }
        let linerefs = lines.iter().map(|s| s.as_str()).collect::<Vec<&str>>();
        return parse_mol2(None, Some(linerefs), false)
    }).into_future().await.unwrap()
}

/// Processes a SMILES string and returns a tuple of atoms, coordinates, and bond data.
///
/// # Arguments
///
/// * `repr` - A string slice representing the SMILES string to be processed.
///
/// # Returns
///
/// A tuple containing:
/// * A vector of `Atom` objects representing the atoms in the molecule described by the SMILES string.
/// * A 2D vector of `f64` values representing the coordinates of the atoms in 3D space.
/// * A vector of `BondData` objects representing the bonds between the atoms.
///
/// # Remarks
///
/// This function reads the SMILES string, adds implicit hydrogens, creates a distance matrix, performs multidimensional scaling to get the coordinates, and gets the bond list. It then returns the atoms, coordinates, and bond data.
pub fn process_smiles(repr: &str) -> (Vec<Atom>, Vec<Vec<f64>>, Vec<BondData>) {
    let mut atoms = read_smiles(repr);
    add_implicit_hydrogens(&mut atoms.borrow_mut());
    let dists = create_distance_matrix(&atoms);
    let coords = mds(&dists, 3);
    let bonds = get_bonds_list(&atoms, &coords);
    (atoms, coords, bonds)
}

/// Represents the data for a bond in a molecule.
///
/// # Fields
///
/// * `count` - The number of bonds of this type.
/// * `tid` - The target atom ID of the bond.
/// * `start` - A tuple representing the starting coordinates of the bond.
/// * `end` - A tuple representing the ending coordinates of the bond.
///
/// # Remarks
///
/// This struct is used to store the data for a bond, including the number of bonds of this type, the target atom ID, and the start and end coordinates. It is used in the `get_bonds_list` function to create a list of bonds for a given set of atoms and their coordinates.
#[derive(Debug)]
pub struct BondData {
    pub count: usize,
    pub tid: usize,
    pub start: (f64, f64, f64),
    pub end: (f64, f64, f64)
}

/// Converts a `BondKind` from purr crate to a bond count as the provided enum doesn't support that.
///
/// # Arguments
///
/// * `kind` - A reference to a `BondKind` enum.
///
/// # Returns
///
/// A `usize` representing the bond count. The bond count is determined by the type of bond. For example, a single bond has a count of 1, a double bond has a count of 2, etc. For aromatic, elided, up, down, and any other unspecified bond kinds, the count is 1.
///
/// # Remarks
///
/// This function is used to convert the bond type to a numerical value that can be used in calculations.
pub fn bond_kind_to_bonds_count(kind: &BondKind) -> usize {
    match kind {
        BondKind::Single => 1,
        BondKind::Double => 2,
        BondKind::Triple => 3,
        BondKind::Quadruple => 4,
        BondKind::Aromatic => 1,
        BondKind::Elided => 1,
        BondKind::Up => 1,
        BondKind::Down => 1,
        _ => 1
    }
}

/// Returns a list of bond data for a given set of atoms and their coordinates.
///
/// # Arguments
///
/// * `atoms` - A vector of `Atom` objects from purr crate representing the atoms in the molecule.
/// * `coords` - A vector of vectors representing the x, y, and z coordinates of the atoms.
///
/// # Returns
///
/// A vector of `BondData` objects. Each `BondData` object contains the following fields:
/// * `count` - The number of bonds of this type.
/// * `tid` - The target atom ID of the bond.
/// * `start` - A tuple representing the starting coordinates of the bond.
/// * `end` - A tuple representing the ending coordinates of the bond.
///
/// # Remarks
///
/// This function iterates over each atom and its bonds. For each bond, it checks if a `BondData` object
/// with the same `tid` and start and end coordinates already exists in the `bonds_list`. If not, it creates
/// a new `BondData` object and adds it to the `bonds_list`.
pub fn get_bonds_list(atoms: &Vec<Atom>, coords: &Vec<Vec<f64>>) -> Vec<BondData> {
    let mut bonds_list: Vec<BondData> = Vec::new();
    for (i, atom) in atoms.iter().enumerate() {
        for bond in &atom.bonds {
            let j = bond.tid as usize;
            let start = (coords[0][i], coords[1][i], coords[2][i]);
            let end = (coords[0][j], coords[1][j], coords[2][j]);
            if bonds_list.iter().find(|b| b.tid == j && b.start == end && b.end == start).is_none() {
                bonds_list.push(BondData {
                    count: bond_kind_to_bonds_count(bond.kind.borrow()),
                    tid: j,
                    start,
                    end
                });
            }
        }
    
    }
    bonds_list
}

/// Parses a SMILES string and returns a vector of `Atom` objects.
///
/// # Arguments
///
/// * `repr` - A string slice representing the SMILES string to be parsed.
///
/// # Returns
///
/// A vector of `Atom` objects representing the atoms in the molecule described by the SMILES string.
///
/// # Remarks
///
/// This function uses the `Builder` struct to create a molecule from the SMILES string. It then converts
/// the molecule into a vector of `Atom` objects. Currently, the function does not handle implicit hydrogens.
/// It's fully based on the purr crate.
pub fn read_smiles(repr: &str) -> Vec<Atom>{
    let mut builder = Builder::new();
    read(repr, &mut builder, None);
    let atoms = builder.build().expect("atoms");
    let atoms_vec : Vec<Atom> = atoms.into_iter().collect();
    // todo!("Append the implicit hydrogens to the atoms_vec");
    atoms_vec
}

/// Creates a distance matrix for a given set of atoms.
///
/// # Arguments
///
/// * `atoms` - A vector of `Atom` objects representing the atoms in the molecule.
///
/// # Returns
///
/// A 2D vector of `u32` values representing the distance matrix. The distance between two atoms is determined by the type of bond between them. The distance is set to `u32::MAX` if there is no direct bond between the atoms.
///
/// # Remarks
///
/// This function first initializes a 2D vector with `u32::MAX`. It then iterates over each atom and its bonds,
/// and sets the distance between the atom and the target atom of each bond based on the bond type.
/// The distances are then updated using the Floyd-Warshall algorithm to find the shortest paths between all pairs of atoms.
/// The bond distances are picked arbitrally right now. This function is used to calculate the distance matrix for the MDS algorithm.
fn create_distance_matrix(atoms: &Vec<Atom>) -> Vec<Vec<u32>> {
    let n = atoms.len();
    let mut dist = vec![vec![u32::MAX; n]; n];

    for i in 0..n {
        dist[i][i] = 0;
        for bond in &atoms[i].bonds {
            let j = bond.tid as usize;
            // Adjust the distance based on the bond type
            let bond_distance = match bond.kind {
                BondKind::Single => 3,  // Single bond
                BondKind::Double => 2,  // Double bond
                BondKind::Triple => 1,  // Triple bond
                BondKind::Quadruple => 1,  // Quadruple bond
                BondKind::Aromatic => 3,  // Aromatic bond
                BondKind::Elided => 1,  // Elided bond
                BondKind::Up => 3,  // Up bond
                BondKind::Down => 3,  // Down bond
                _ => 3,
            };
            dist[i][j] = bond_distance;
            dist[j][i] = bond_distance;
        }
    }

    for k in 0..n {
        for i in 0..n {
            for j in 0..n {
                if dist[i][k] != u32::MAX && dist[k][j] != u32::MAX {
                    dist[i][j] = dist[i][j].min(dist[i][k] + dist[k][j]);
                }
            }
        }
    }

    dist
}

/// Performs Multidimensional Scaling (MDS) on a given distance matrix.
///
/// # Arguments
///
/// * `dists` - A 2D vector of `u32` values representing the distance matrix.
/// * `dimensions` - The number of dimensions for the output coordinates.
///
/// # Returns
///
/// A 2D vector of `f64` values representing the coordinates of the points in the specified number of dimensions.
///
/// # Remarks
///
/// This function uses the classical MDS algorithm, which involves the following steps:
/// 1. Compute the centering matrix H.
/// 2. Compute the matrix B = -1/2 * H * D * H.
/// 3. Compute the eigenvalues and eigenvectors of B.
/// 4. Select the largest `dimensions` eigenvalues and their corresponding eigenvectors.
/// 5. The coordinates are given by the matrix product of the matrix of selected eigenvectors and the square root of the diagonal matrix of selected eigenvalues.
///
/// The implementation is based on: https://www.stat.pitt.edu/sungkyu/course/2221Fall13/lec8_mds_combined.pdf
pub fn mds(dists: &Vec<Vec<u32>>, dimensions: usize) -> Vec<Vec<f64>> {
    let n = dists.len();

    let dists_f64: DMatrix<f64> = DMatrix::from_fn(n, n, |i, j| dists[i][j] as f64);
    let h = DMatrix::from_fn(n, n, |i, j| if i == j { 1.0 - 1.0/n as f64 } else { -1.0/n as f64 });
    let b = &h * dists_f64 * &h * -0.5;
    let SymmetricEigen { mut eigenvectors, mut eigenvalues } = b.symmetric_eigen();
    let mut eigen_pairs: Vec<(f64, Vec<f64>)> = eigenvalues.iter().zip(eigenvectors.column_iter()).map(|(&eigenvalue, eigenvector)| (eigenvalue, eigenvector.iter().cloned().collect())).collect();
    eigen_pairs.sort_by(|a, b| b.0.partial_cmp(&a.0).unwrap());
    eigen_pairs.truncate(dimensions);

    let coords: Vec<Vec<f64>> = eigen_pairs.iter().map(|(eigenvalue, eigenvector)| {
        eigenvector.iter().map(|&x| x * eigenvalue.sqrt() * 5.0).collect()
    }).collect();

    coords
}

#[cfg(test)]
mod tests {
    use purr::feature::{AtomKind, Element};

    use super::*;

    #[test]
    fn test_process_smiles() {
        let (atoms, coords, bonds) = process_smiles("C([C@@H]([C@@H]1C(=O)C(=C(O1)O)O)O)O");
        assert_eq!(atoms.len(), 20);
        assert_eq!(coords.len(), 3);
        assert_eq!(bonds.len(), 32);
    }

    #[test]
    fn test_bond_kind_to_bonds_count() {
        let kind = BondKind::Single;
        let count = bond_kind_to_bonds_count(&kind);
        assert_eq!(count, 1);

        let kind = BondKind::Double;
        let count = bond_kind_to_bonds_count(&kind);
        assert_eq!(count, 2);

        let kind = BondKind::Triple;
        let count = bond_kind_to_bonds_count(&kind);
        assert_eq!(count, 3);

        let kind = BondKind::Quadruple;
        let count = bond_kind_to_bonds_count(&kind);
        assert_eq!(count, 4);

        let kind = BondKind::Aromatic;
        let count = bond_kind_to_bonds_count(&kind);
        assert_eq!(count, 1);

        let kind = BondKind::Elided;
        let count = bond_kind_to_bonds_count(&kind);
        assert_eq!(count, 1);
    }
}