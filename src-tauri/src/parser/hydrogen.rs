use purr::{feature::BracketSymbol, graph::Atom, feature::AtomKind, feature::Element, feature::BondKind, graph::Bond};

use super::get_atom_symbol;

/// Adds implicit hydrogen atoms to a given set of atoms.
///
/// # Arguments
///
/// * `atoms` - A mutable reference to a vector of `Atom` objects representing the atoms in the molecule.
///
/// # Remarks
///
/// This function iterates over each atom in the given vector. If the atom is not a hydrogen atom and has suppressed hydrogens, it creates the corresponding number of hydrogen `Atom` objects and adds them to the vector. The hydrogen atoms are created with a `BondKind::Elided` bond to the original atom.
///
/// The function modifies the given vector in-place. It does not return a value.
pub fn add_implicit_hydrogens(atoms: &mut Vec<Atom>) {
    let mut hydrogens: Vec<Atom> = vec![];
    for (i, atom) in atoms.iter().enumerate() {
        if get_atom_symbol(atom) == "H" {
            continue;
        }
        let h_count = atom.suppressed_hydrogens();
        let id = i;
        if h_count > 0 {
            for _ in 0..h_count {
                hydrogens.push(Atom{
                    kind: AtomKind::Bracket{
                        symbol: BracketSymbol::Element((Element::H)),
                        hcount: None,
                        charge: None,
                        isotope: None,
                        configuration: None,
                        map: None
                    },
                    bonds: vec![
                        Bond::new(BondKind::Elided, id)
                    ]
                });
            }
        }
    }
    atoms.append(&mut hydrogens);
}