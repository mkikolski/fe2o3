use purr::graph::{Atom, Bond};
use purr::feature::{AtomKind, BondKind, Aromatic, BracketSymbol, Element, Configuration, Aliphatic};
use std::fmt::format;
use std::fs::File;
use std::io::{BufReader, Read};
use regex::Regex;
use lazy_static::lazy_static;

use crate::BondData;

/// Defines a set of regular expressions for atom symbols for conversion from plaintext to purr::graph::Atom objects.
/// 
/// # Remarks
/// 
/// Each reference is initialized after the first access as a lazy loading approach to avoid unnecessary memory usage.
lazy_static! {
    static ref AROMATIC_C: Regex = Regex::new(r"C\.ar").unwrap();
    static ref C: Regex = Regex::new(r"C\.[A-Za-z0-9]+").unwrap();
    static ref H: Regex = Regex::new(r"H").unwrap();
    static ref O: Regex = Regex::new(r"O\.[A-Za-z0-9]+").unwrap();
    static ref AROMATIC_N : Regex = Regex::new(r"N\.ar").unwrap();
    static ref N: Regex = Regex::new(r"N\.[A-Za-z0-9]+").unwrap();
    static ref S: Regex = Regex::new(r"S\.[A-Za-z0-9]+").unwrap();
    static ref AL: Regex = Regex::new(r"Al").unwrap();
    static ref BR: Regex = Regex::new(r"Br").unwrap();
    static ref CA: Regex = Regex::new(r"Ca").unwrap();
    static ref CL: Regex = Regex::new(r"Cl").unwrap();
    static ref DU: Regex = Regex::new(r"Du").unwrap();
    static ref F: Regex = Regex::new(r"F").unwrap();
    static ref H_CONF: Regex = Regex::new(r"H\.[A-Za-z0-9]+").unwrap();
    static ref I: Regex = Regex::new(r"I").unwrap();
    static ref K: Regex = Regex::new(r"K").unwrap();
    static ref LI: Regex = Regex::new(r"Li").unwrap();
    static ref NA: Regex = Regex::new(r"Na").unwrap();
    static ref SI: Regex = Regex::new(r"Si").unwrap();
}

/// Loads a file from a given path and returns its contents as a string.
///
/// # Arguments
///
/// * `file_path` - A string slice representing the path to the file to be loaded.
///
/// # Returns
///
/// A `String` containing the contents of the file.
///
/// # Panics
///
/// This function will panic if it fails to open the file or read its contents.
///
/// # Remarks
///
/// This function is used to load a file into memory for further processing. It uses the `BufReader` struct to read the file efficiently.
pub fn load_file(file_path: &str) -> String {
    let file = File::open(file_path).expect("Failed to open file");
    let mut reader = BufReader::new(file);

    let mut contents = String::new();
    reader.read_to_string(&mut contents).expect("Failed to read file");
    // reader.read_to_string(&mut contents).expect("Failed to read file");

    contents
}

/// Parses a MOL2 file and returns the atoms, coordinates, and bond data.
///
/// # Arguments
///
/// * `file_path` - A string slice representing the path to the MOL2 file to be parsed.
///
/// # Returns
///
/// A tuple containing:
/// * A vector of `Atom` objects representing the atoms in the molecule described by the MOL2 file.
/// * A 2D vector of `f64` values representing the coordinates of the atoms in 3D space.
/// * A vector of `BondData` objects representing the bonds between the atoms.
///
/// # Panics
///
/// This function will panic if it encounters an unknown atom symbol in the MOL2 file, but it's unlikely as the match expression contains all the symbols supported by *.mol2 files.
///
/// # Remarks
///
/// This function reads the MOL2 file, parses the atoms and their coordinates, and creates the bond data. It uses regular expressions to match the atom symbols to their corresponding `AtomKind` values.
pub fn parse_mol2(file_path: Option<&str>, parsed_lines: Option<Vec<&str>>, use_filepath: bool) -> (Vec<Atom>, Vec<Vec<f64>>, Vec<BondData>) {
    if use_filepath && file_path.is_none() {
        panic!("File path is required to read the file.");
    }
    if !use_filepath && parsed_lines.is_none() {
        panic!("Lines are required to parse the file.");
    }

    let mut atoms: Vec<Atom> = vec![];
    let mut bonds: Vec<BondData> = vec![];
    let mut coords_x: Vec<f64> = vec![];
    let mut coords_y: Vec<f64> = vec![];
    let mut coords_z: Vec<f64> = vec![];

    let contents;
    let lines = match use_filepath {
        true => {
            contents = load_file(file_path.unwrap());
            readlines(&contents)
        },
        false => parsed_lines.unwrap()
    };
    let mut carret = lines.iter().position(|line| line.contains("@<TRIPOS>ATOM")).unwrap() + 1;

    while !lines[carret].contains("@<TRIPOS>BOND") {
        let mut atom_data: Vec<&str> = lines[carret].split_whitespace().collect();
        let x: f64 = atom_data[2].parse().unwrap();
        let y: f64 = atom_data[3].parse().unwrap();
        let z: f64 = atom_data[4].parse().unwrap();
        let symbol = atom_data[5];
        let atom_kind = match symbol {
            _ if AROMATIC_C.is_match(symbol) => AtomKind::Aromatic((Aromatic::C)),
            _ if H.is_match(symbol) => AtomKind::Bracket{
                symbol: BracketSymbol::Element((Element::H)),
                hcount: None,
                charge: None,
                isotope: None,
                configuration: None,
                map: None
            },
            _ if C.is_match(symbol) => AtomKind::Aliphatic((Aliphatic::C)),
            _ if O.is_match(symbol) => AtomKind::Aliphatic((Aliphatic::O)),
            _ if AROMATIC_N.is_match(symbol) => AtomKind::Aromatic((Aromatic::N)),
            _ if N.is_match(symbol) => AtomKind::Aliphatic((Aliphatic::N)),
            _ if S.is_match(symbol) => AtomKind::Aliphatic((Aliphatic::S)),
            _ => panic!("{}", format(format_args!("Unknown atom symbol: {}", symbol)))
        };
        let atom = Atom::new(atom_kind);
        atoms.push(atom);
        coords_x.push(x);
        coords_y.push(y);
        coords_z.push(z);
        carret += 1;
    }
    carret += 1;

    while carret < lines.len() && !lines[carret].contains("@<TRIPOS>SUBSTRUCTURE"){
        let mut bond_data: Vec<&str> = lines[carret].split_whitespace().collect();
        let start: usize = bond_data[1].parse::<usize>().unwrap() - 1;
        let end: usize = bond_data[2].parse::<usize>().unwrap() - 1;
        let btype = bond_data[3];
        let ct = match btype {
            "1" => 1,
            "2" => 2,
            "3" => 3,
            "ar" => 1,
            "am" => 1,
            "du" => 1,
            "un" => 1,
            "nc" => panic!("No support for not connected bond type. Fix your mol2 file."),
            _ => panic!("{}", format(format_args!("Unknown bond type: {}", btype)))
        } as usize;
        bonds.push(BondData {
            count: ct,
            tid: end,
            start: (coords_x[start], coords_y[start], coords_z[start]),
            end: (coords_x[end], coords_y[end], coords_z[end])
        });
        carret += 1;
    }

    offset_bonds(&mut bonds);

    (atoms, vec![coords_x, coords_y, coords_z], bonds)
}

/// Offsets the start and end points of each bond in a vector of `BondData` objects based on the bond count.
///
/// # Arguments
///
/// * `bonds` - A mutable reference to a vector of `BondData` objects.
///
/// # Panics
///
/// This function will panic if the bond count is not 1, 2, or 3, indicating that the data may have been corrupted.
///
/// # Remarks
///
/// This function is used to adjust the start and end points of each bond in a molecule based on the bond count. If the bond count is 2, the function creates an additional bond with the start and end points offset by -0.2. If the bond count is 3, the function creates two additional bonds with the start and end points offset by -0.2 and 0.2, respectively. The additional bonds are appended to the original vector of bonds.
fn offset_bonds(bonds: &mut Vec<BondData>) {
    let mut additional_bonds: Vec<BondData> = vec![];
    for bond in bonds.iter_mut() {
        match bond.count {
            1 => continue,
            2 => {
                additional_bonds.push(BondData {
                    count: 1,
                    tid: bond.tid,
                    start: offset_point(&bond.start, -0.2),
                    end: offset_point(&bond.end, -0.2)
                });
                bond.start = offset_point(&bond.start, 0.2);
                bond.end = offset_point(&bond.end, 0.2);
            },
            3 => {
                additional_bonds.push(BondData {
                    count: 1,
                    tid: bond.tid,
                    start: offset_point(&bond.start, -0.2),
                    end: offset_point(&bond.end, -0.2)
                });
                additional_bonds.push(BondData {
                    count: 1,
                    tid: bond.tid,
                    start: offset_point(&bond.start, 0.2),
                    end: offset_point(&bond.end, 0.2)
                });
            },
            _ => panic!("It shouldn't be called anytime. If called, it's possible that data has been corrupted as some point.") 
        }
    }
    bonds.append(&mut additional_bonds);
}

/// Offsets a point in 3D space along the z-axis by a given amount.
///
/// # Arguments
///
/// * `point` - A reference to a tuple representing the (x, y, z) coordinates of the point.
/// * `offset` - A `f64` value representing the amount to offset the point along the z-axis.
///
/// # Returns
///
/// A tuple representing the (x, y, z) coordinates of the new point.
///
/// # Remarks
///
/// This function is used to offset a point in 3D space along the z-axis. It is used in the `offset_bonds` function to adjust the start and end points of each bond in a molecule based on the bond count.
fn offset_point(point: &(f64, f64, f64), offset: f64) -> (f64, f64, f64) {
    let mut new_point = *point;
    new_point.2 += offset;
    new_point
}

/// Splits the contents of a string into lines and returns them as a vector.
///
/// # Arguments
///
/// * `contents` - A string slice representing the contents to be split into lines.
///
/// # Returns
///
/// A vector of string slices, each representing a line in the original string.
///
/// # Remarks
///
/// This function is used to split the contents of a file or other string into lines for further processing. It uses the `lines` method of the `str` type, which returns an iterator over the lines of the string, and the `collect` method to convert this iterator into a vector.
fn readlines(contents: &str) -> Vec<&str> {
    contents.lines().collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_load_file() {
        let file_path = "/home/mkikolski/fe2o3/fe2o3/src-tauri/data/test.txt";
        let contents = load_file(file_path);
        println!("{}", contents);
        assert_eq!(contents, "This is a test file\nthe contents are here to check if the program\nis capable of loading me!");
    }

    #[test]
    #[should_panic]
    fn test_load_file_panic() {
        let file_path = "/home/mkikolski/fe2o3/fe2o3/src-tauri/data/test2.txt"; // this file doesn't exist on purpose
        let _contents = load_file(file_path);
    }

    #[test]
    fn test_readlines() {
        let contents = "This is a test file\nthe contents are here to check if the program\nis capable of loading me!";
        let lines = readlines(contents);
        assert_eq!(lines, vec!["This is a test file", "the contents are here to check if the program", "is capable of loading me!"]);
    }

    #[test]
    fn test_offset_point() {
        let point = (1.0, 2.0, 3.0);
        let offset = 0.5;
        let new_point = offset_point(&point, offset);
        assert_eq!(new_point, (1.0, 2.0, 3.5));
    }
}