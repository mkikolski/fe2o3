use std::{error::Error, fmt::format};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Compound {
    pub name: String,
    pub smiles: String,
    pub cid: String,
    pub image_url: String
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ListOfCompounds {
    pub compounds: Vec<Compound>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PubChemError {
    pub message: String
}

impl From<reqwest::Error> for PubChemError {
    fn from(error: reqwest::Error) -> Self {
        PubChemError {
            message: error.to_string()
        }
    }
}

pub async fn get_compounds_list_by_name(name: &str) -> Result<Vec<String>, PubChemError> {
    let url = format!("https://pubchem.ncbi.nlm.nih.gov/rest/pug/substance/name/{}/cids/XML", name);
    let response = reqwest::get(&url).await?.text().await?;
    let mut cids: Vec<String> = vec![];
    for line in response.lines() {
        if line.contains("<CID>") {
            let stripped1 = line.replace("<CID>", "");
            let stripped2 = stripped1.replace("</CID>", "");
            let cid = stripped2.trim().to_string();
            
            if !cids.contains(&cid) {
                cids.push(cid);
            }
        }
    }
    Ok(cids)
}

pub async fn get_compound_data_by_cid(cid: &str) -> Result<Compound, PubChemError> {
    let url = format!("https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/{}/record/XML", cid);
    let response = reqwest::get(&url).await?.text().await?;
    let mut name = String::new();
    let mut smiles = String::new();
    let mut found_name_flag_major = false;
    let mut found_name_flag_minor = false;
    let mut skip_names_flag = false;
    let mut found_smiles_flag_major = false;
    let mut found_smiles_flag_minor = false;
    let mut skip_smiles_flag = false;
    for line in response.lines() {
        if line.contains("IUPAC Name") && !skip_names_flag {
            // println!("[LOG]: Line found_name_flag_major: {}", line);
            found_name_flag_major = true;
        } else if line.contains("Preferred") && found_name_flag_major && !skip_names_flag {
            // println!("[LOG]: Line found_name_flag_minor: {}", line);
            found_name_flag_minor = true;
        } else if line.contains("<PC-InfoData_value_sval>") && found_name_flag_major && found_name_flag_minor && !skip_names_flag {
            // println!("[LOG]: Line found_name: {}", line);
            let stripped1 = line.replace("<PC-InfoData_value_sval>", "");
            let stripped2 = stripped1.replace("</PC-InfoData_value_sval>", "");
            name = stripped2.trim().to_string();
            skip_names_flag = true;
        } else if line.contains("SMILES") && !skip_smiles_flag {
            // println!("[LOG]: Line found_smiles_flag_major: {}", line);
            found_smiles_flag_major = true;
        } else if line.contains("Canonical") && found_smiles_flag_major && !skip_smiles_flag {
            // println!("[LOG]: Line found_smiles_flag_minor: {}", line);
            found_smiles_flag_minor = true;
        } else if line.contains("<PC-InfoData_value_sval>") && found_smiles_flag_major && found_smiles_flag_minor && !skip_smiles_flag {
            // println!("[LOG]: Line found_smiles: {}", line);
            let stripped1 = line.replace("<PC-InfoData_value_sval>", "");
            let stripped2 = stripped1.replace("</PC-InfoData_value_sval>", "");
            smiles = stripped2.trim().to_string();
            skip_smiles_flag = true;
        } else if skip_names_flag && skip_smiles_flag {
            break;
        }
    }
    let img_url = format!("https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/{}/record/PNG", cid);
    Ok(Compound {
        name: name, smiles: smiles, cid: cid.to_string(), image_url: img_url
    })
}

pub async fn load_compounds_data_from_name(name: &str) -> Result<ListOfCompounds, PubChemError> {
    let cids = get_compounds_list_by_name(name).await?;
    let mut compounds: Vec<Compound> = vec![];
    for cid in cids {
        let compound = get_compound_data_by_cid(&cid).await?;
        compounds.push(compound);
    }
    Ok(ListOfCompounds {compounds})
}