use serde::ser::Error;

use crate::{Compound, ListOfCompounds, PubChemError};
use crate::pubchem_api as pubchem_api;

#[tauri::command]
pub async fn load_compounds_from_name(name: &str) -> Result<ListOfCompounds, PubChemError> {
    pubchem_api::load_compounds_data_from_name(name).await
}