// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

mod model_generation;
mod parser;
mod fileformat;
mod save_file;
mod pubchem_api;
use std::io::Error;

use model_generation::*;
use parser::*;
use fileformat::*;
use pubchem_api::*;
use purr::graph::Atom;
use parser::get_atom_symbol;
use tauri::AppHandle;
use crate::save_file::save_image;

/// Represents the response sent to the Tauri frontend.
///
/// # Fields
///
/// * `sphere_coords` - A vector of vectors of `f32` values representing the coordinates of the spheres (atoms) in the molecule.
/// * `sphere_ids` - A vector of vectors of `Index` values representing the indices of the spheres (atoms) in the molecule.
/// * `colors` - A vector of `String` values representing the colors of the spheres (atoms) in the molecule.
/// * `bond_pairs` - A vector of tuples representing the pairs of coordinates for the bonds in the molecule. Each tuple contains two tuples, each of which represents the (x, y, z) coordinates of an atom.
///
/// # Remarks
///
/// This struct is used to send data about a molecule to the Tauri frontend. The data includes the coordinates and colors of the atoms, as well as the pairs of coordinates for the bonds. The struct is serializable, which allows it to be easily converted to JSON.
#[derive(serde::Serialize)]
struct TauriMoleculeResponse {
    sphere_coords: Vec<Vec<f32>>,
    sphere_ids: Vec<Vec<Index>>,
    colors: Vec<String>,
    bond_pairs: Vec<((f32, f32, f32), (f32, f32, f32))>
}

/// Creates a molecule from a string representation and returns a `TauriMoleculeResponse`.
///
/// # Arguments
///
/// * `repr` - A string representing the molecule. This can be in SMILES format or a path to mol2 file, depending on the value of `mol2_mode`.
/// * `mol2_mode` - A boolean indicating whether the molecule representation is in mol2 format. If `true`, the function uses `parse_mol2` to parse the representation; if `false`, it uses `process_smiles`.
///
/// # Returns
///
/// A `Result` containing a `TauriMoleculeResponse` if the molecule was successfully created, or a `String` error message otherwise.
///
/// # Remarks
///
/// This function is used to create a molecule from a string representation and send the data to the Tauri frontend. It loads the color settings for the atoms, generates the spheres (atoms) and bonds of the molecule, and creates a `TauriMoleculeResponse` with the data.
#[tauri::command]
async fn create_molecule(repr: &str, mol2_mode: bool, handle: tauri::AppHandle) -> Result<TauriMoleculeResponse, String> { // (Vec<Atom>, Vec<Vec<f32>>, Vec<Vec<Index>>)
    let color_settings = load_color_settings(handle);
    let mut sphere_coords : Vec<Vec<f32>> = vec![];
    let mut sphere_ids : Vec<Vec<Index>> = vec![];
    let mut bond_pairs : Vec<((f32, f32, f32), (f32, f32, f32))> = vec![];
    let mut colors: Vec<String> = vec![];
    let (atoms, coords, bonds) = match mol2_mode {
        true => parse_mol2(Some(repr), None, true),
        false => run_openbabel(repr).await 
    };
    for (i, atom) in atoms.iter().enumerate() {
        let (sphere, ids) = generate_sphere(get_atom_radius(atom, Some(0.007)), 16, (coords[0][i] as f32, coords[1][i] as f32, coords[2][i] as f32));
        sphere_coords.push(sphere);
        sphere_ids.push(ids);
        colors.push(color_settings.get(&get_atom_symbol(&atom)).unwrap().to_string());
    }
    for bond in bonds {
        bond_pairs.push(((bond.start.0 as f32, bond.start.1 as f32, bond.start.2 as f32), (bond.end.0 as f32, bond.end.1 as f32, bond.end.2 as f32)));
    }
    Ok(TauriMoleculeResponse{sphere_coords, sphere_ids, colors, bond_pairs})
}


fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![create_molecule, save_fe2o3, load_fe2o3, save_image, load_compounds_from_name])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");

    // let (atoms, _) = process_smiles("C([C@@H]([C@@H]1C(=O)C(=C(O1)O)O)O)O");
    // for atom in atoms {
    //     println!("{:?}", get_atom_symbol(&atom));
    // }

    // let (atoms, coords, bonds) = parse_mol2("/home/mkikolski/fe2o3/fe2o3/src-tauri/data/testfile.mol2");
    // println!("{:?}", atoms);
    // println!("{:?}", coords);
    // println!("{:?}", bonds);
}


