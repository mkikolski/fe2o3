use crate::SavedApplicationState;

#[tauri::command]
pub async fn save_fe2o3(application_state: SavedApplicationState, path: String, handle: tauri::AppHandle) -> Result<(), String> {
    SavedApplicationState::save_to_file(&application_state, &path).map_err(|e| e.to_string())?;
    Ok(())
}

#[tauri::command]
pub async fn load_fe2o3(path: String) -> Result<SavedApplicationState, String> {
    SavedApplicationState::load_from_file(&path).map_err(|e| e.to_string())
}