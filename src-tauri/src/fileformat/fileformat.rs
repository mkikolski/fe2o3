use serde::{Serialize, Deserialize};
use std::io::{self, Write, Read};
use std::fs::File;

#[derive(Serialize, Deserialize)]
pub struct Sphere {
    coords: Vec<f32>,
    ids: Vec<u32>,
    color: String
}

#[derive(Serialize, Deserialize)]
pub struct SavedApplicationState {
    spheres: Vec<Sphere>,
    bond_pairs: Vec<((f32, f32, f32), (f32, f32, f32))>,
    environment: String,
    environment_blur: f32,
    show_background: bool,
    bg_intensity: f32,
    bg_rotation: f32,
    ambient_light: f32,
    bond_thickness: f32,
    show_bonds: bool,
    apply_scale: bool,
    scale_factor: f32,
    roughness: f32,
    detail: u8,
    space_scale: f32,
    bond_color: String,
    fov: f32
}

impl Sphere {
    fn serialize(&self) -> Vec<u8> {
        let mut bytes = Vec::new();
        
        // Serialize coords
        let coords_length = self.coords.len() as u32;
        bytes.extend(&coords_length.to_le_bytes());
        for &coord in &self.coords {
            bytes.extend(&coord.to_le_bytes());
        }

        // Serialize ids
        let ids_length = self.ids.len() as u32;
        bytes.extend(&ids_length.to_le_bytes());
        for &id in &self.ids {
            bytes.extend(&id.to_le_bytes());
        }

        // Serialize color
        let color_bytes = self.color.as_bytes();
        let color_length = color_bytes.len() as u32;
        bytes.extend(&color_length.to_le_bytes());
        bytes.extend(color_bytes);

        bytes
    }

    fn deserialize(bytes: &[u8], offset: &mut usize) -> Self {
        // Deserialize coords
        let coords_length = u32::from_le_bytes(bytes[*offset..*offset+4].try_into().unwrap()) as usize;
        *offset += 4;
        let mut coords = Vec::with_capacity(coords_length);
        for _ in 0..coords_length {
            let coord = f32::from_le_bytes(bytes[*offset..*offset+4].try_into().unwrap());
            coords.push(coord);
            *offset += 4;
        }

        // Deserialize ids
        let ids_length = u32::from_le_bytes(bytes[*offset..*offset+4].try_into().unwrap()) as usize;
        *offset += 4;
        let mut ids = Vec::with_capacity(ids_length);
        for _ in 0..ids_length {
            let id = u32::from_le_bytes(bytes[*offset..*offset+4].try_into().unwrap());
            ids.push(id);
            *offset += 4;
        }

        // Deserialize color
        let color_length = u32::from_le_bytes(bytes[*offset..*offset+4].try_into().unwrap()) as usize;
        *offset += 4;
        let color = String::from_utf8(bytes[*offset..*offset+color_length].to_vec()).unwrap();
        *offset += color_length;

        Sphere { coords, ids, color }
    }
}


impl SavedApplicationState {
    fn serialize(&self) -> Vec<u8> {
        let mut bytes = Vec::new();

        let spheres_length = self.spheres.len() as u32;
        bytes.extend(&spheres_length.to_le_bytes());
        for sphere in &self.spheres {
            bytes.extend(sphere.serialize());
        }

        let bond_pairs_length = self.bond_pairs.len() as u32;
        bytes.extend(&bond_pairs_length.to_le_bytes());
        for &((x1, y1, z1), (x2, y2, z2)) in &self.bond_pairs {
            bytes.extend(&x1.to_le_bytes());
            bytes.extend(&y1.to_le_bytes());
            bytes.extend(&z1.to_le_bytes());
            bytes.extend(&x2.to_le_bytes());
            bytes.extend(&y2.to_le_bytes());
            bytes.extend(&z2.to_le_bytes());
        }

        let environment_bytes = self.environment.as_bytes();
        let environment_length = environment_bytes.len() as u32;
        bytes.extend(&environment_length.to_le_bytes());
        bytes.extend(environment_bytes);

        bytes.extend(&self.environment_blur.to_le_bytes());

        bytes.push(self.show_background as u8);

        bytes.extend(&self.bg_intensity.to_le_bytes());

        bytes.extend(&self.bg_rotation.to_le_bytes());

        bytes.extend(&self.ambient_light.to_le_bytes());

        bytes.extend(&self.bond_thickness.to_le_bytes());

        bytes.push(self.show_bonds as u8);

        bytes.push(self.apply_scale as u8);

        bytes.extend(&self.scale_factor.to_le_bytes());

        bytes.extend(&self.roughness.to_le_bytes());

        bytes.push(self.detail);

        bytes.extend(&self.space_scale.to_le_bytes());

        let bond_color_bytes = self.bond_color.as_bytes();
        let bond_color_length = bond_color_bytes.len() as u32;
        bytes.extend(&bond_color_length.to_le_bytes());
        bytes.extend(bond_color_bytes);

        bytes.extend(&self.fov.to_le_bytes());

        bytes
    }

    fn deserialize(bytes: &[u8]) -> Self {
        let mut offset = 0;

        let spheres_length = u32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap()) as usize;
        offset += 4;
        let mut spheres = Vec::with_capacity(spheres_length);
        for _ in 0..spheres_length {
            let sphere = Sphere::deserialize(bytes, &mut offset);
            spheres.push(sphere);
        }

        let bond_pairs_length = u32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap()) as usize;
        offset += 4;
        let mut bond_pairs = Vec::with_capacity(bond_pairs_length);
        for _ in 0..bond_pairs_length {
            let x1 = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
            offset += 4;
            let y1 = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
            offset += 4;
            let z1 = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
            offset += 4;
            let x2 = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
            offset += 4;
            let y2 = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
            offset += 4;
            let z2 = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
            offset += 4;
            bond_pairs.push(((x1, y1, z1), (x2, y2, z2)));
        }

        let environment_length = u32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap()) as usize;
        offset += 4;
        let environment = String::from_utf8(bytes[offset..offset+environment_length].to_vec()).unwrap();
        offset += environment_length;

        let environment_blur = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
        offset += 4;

        let show_background = bytes[offset] != 0;
        offset += 1;

        let bg_intensity = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
        offset += 4;

        let bg_rotation = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
        offset += 4;

        let ambient_light = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
        offset += 4;

        let bond_thickness = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
        offset += 4;

        let show_bonds = bytes[offset] != 0;
        offset += 1;

        let apply_scale = bytes[offset] != 0;
        offset += 1;

        let scale_factor = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
        offset += 4;

        let roughness = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
        offset += 4;

        let detail = bytes[offset];
        offset += 1;

        let space_scale = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
        offset += 4;

        let bond_color_length = u32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap()) as usize;
        offset += 4;
        let bond_color = String::from_utf8(bytes[offset..offset+bond_color_length].to_vec()).unwrap();
        offset += bond_color_length;

        let fov = f32::from_le_bytes(bytes[offset..offset+4].try_into().unwrap());
        offset += 4;

        SavedApplicationState {
            spheres,
            bond_pairs,
            environment,
            environment_blur,
            show_background,
            bg_intensity,
            bg_rotation,
            ambient_light,
            bond_thickness,
            show_bonds,
            apply_scale,
            scale_factor,
            roughness,
            detail,
            space_scale,
            bond_color,
            fov,
        }
    }
}

impl SavedApplicationState {
    pub fn save_to_file(&self, filename: &str) -> io::Result<()> {
        let bytes = self.serialize();
        let mut file = File::create(filename)?;
        file.write_all(&bytes)?;
        Ok(())
    }

    pub fn load_from_file(filename: &str) -> io::Result<Self> {
        let mut file = File::open(filename)?;
        let mut bytes = Vec::new();
        file.read_to_end(&mut bytes)?;
        Ok(Self::deserialize(&bytes))
    }
}