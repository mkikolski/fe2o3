use std::any::Any;
use std::fmt::{format, Debug, Formatter};
use std::fs::File;
use std::io::Write;
use std::path::Path;
use purr::graph::Atom;
use purr::feature::AtomKind;

// pub struct Molecule {
//     pub vertices: Vec<Vertex>,
//     pub indices: Vec<Index>,
//     pub symbol: String,
//     pub location: [f32; 3],
// }

// // Define the Vertex and Index types
// #[derive(Debug, Clone, Copy)]
// pub struct Vertex {
//     pub position: [f32; 3],
// }

// Type introduced for better code readability
pub type Index = u32;

/// Generates the vertices and indices for a sphere mesh.
///
/// # Arguments
///
/// * `radius` - The radius of the sphere.
/// * `segments` - The number of segments to divide the sphere into. More segments result in a smoother sphere.
/// * `center` - A tuple representing the (x, y, z) coordinates of the center of the sphere.
///
/// # Returns
///
/// A tuple containing:
/// * A vector of `f32` values representing the vertices of the sphere. Each group of three consecutive values represents the (x, y, z) coordinates of a vertex.
/// * A vector of `Index` values representing the indices of the vertices. Each group of three consecutive values represents a triangle.
///
/// # Remarks
///
/// This function uses spherical coordinates to generate the vertices of the sphere. It then generates the indices by connecting the vertices to form triangles. The resulting mesh can be used to create a 3D model of a sphere in a graphics library such as ThreeJS.
pub fn generate_sphere(radius: f32, segments: u32, center: (f32, f32, f32)) -> (Vec<f32>, Vec<Index>) {
    let mut vertices = Vec::new();
    let mut indices = Vec::new();

    for i in 0..=segments {
        for j in 0..=segments {
            let x = center.0 + radius * (i as f32 * std::f32::consts::PI / segments as f32).sin() * (j as f32 * 2.0 * std::f32::consts::PI / segments as f32).cos();
            let y = center.1 + radius * (i as f32 * std::f32::consts::PI / segments as f32).cos();
            let z = center.2 + radius * (i as f32 * std::f32::consts::PI / segments as f32).sin() * (j as f32 * 2.0 * std::f32::consts::PI / segments as f32).sin();

            vertices.push(x);
            vertices.push(y);
            vertices.push(z);

            if i < segments && j < segments {
                let a = i * (segments + 1) + j;
                let b = a + segments + 1;
                let c = a + 1;
                let d = b + 1;
                indices.push(a as Index);
                indices.push(b as Index);
                indices.push(c as Index);
                indices.push(b as Index);
                indices.push(d as Index);
                indices.push(c as Index);
            }
        }
    }

    (vertices, indices)
}

/// Generates the vertices and indices for a cylinder mesh.
///
/// # Arguments
///
/// * `radius` - The radius of the base of the cylinder.
/// * `base1` - A tuple representing the (x, y, z) coordinates of the center of one base of the cylinder.
/// * `base2` - A tuple representing the (x, y, z) coordinates of the center of the other base of the cylinder.
/// * `segments` - The number of segments to divide the base of the cylinder into. More segments result in a smoother cylinder.
///
/// # Returns
///
/// A tuple containing:
/// * A vector of `f32` values representing the vertices of the cylinder. Each group of three consecutive values represents the (x, y, z) coordinates of a vertex.
/// * A vector of `Index` values representing the indices of the vertices. Each group of three consecutive values represents a triangle.
///
/// # Remarks
///
/// This function generates the vertices of the cylinder by rotating a point around the axis of the cylinder. It then generates the indices by connecting the vertices to form triangles.
#[allow(dead_code)]
pub fn generate_cylinder(radius: f32, base1: (f32, f32, f32), base2: (f32, f32, f32), segments: u32) -> (Vec<f32>, Vec<Index>) {
    let mut vertices = Vec::new();
    let mut indices = Vec::new();

    let direction = (
        base2.0 - base1.0,
        base2.1 - base1.1,
        base2.2 - base1.2,
    );

    let height = (direction.0.powi(2) + direction.1.powi(2) + direction.2.powi(2)).sqrt();

    let direction = (
        direction.0 / height,
        direction.1 / height,
        direction.2 / height,
    );

    for i in 0..=segments {
        let angle = i as f32 * 2.0 * std::f32::consts::PI / segments as f32;
        let x = radius * angle.cos();
        let z = radius * angle.sin();

        let rotated_x = direction.0 * x - direction.2 * z;
        let rotated_z = direction.0 * z + direction.2 * x;

        vertices.push(base1.0 + rotated_x);
        vertices.push(base1.1 + height / 2.0);
        vertices.push(base1.2 + rotated_z);

        vertices.push(base1.0 + rotated_x);
        vertices.push(base1.1 - height / 2.0);
        vertices.push(base1.2 + rotated_z);

        if i < segments {
            let a = i * 2;
            let b = a + 1;
            let c = (i + 1) * 2;
            let d = c + 1;
            indices.push(a as Index);
            indices.push(b as Index);
            indices.push(c as Index);
            indices.push(b as Index);
            indices.push(d as Index);
            indices.push(c as Index);
        }
    }

    (vertices, indices)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_generate_sphere() {
        let (vertices, indices) = generate_sphere(1.0, 16, (0.0, 0.0, 0.0));
        assert_eq!(vertices.len(), 867);
        assert_eq!(indices.len(), 1536);
    }
}
