import { useState, useEffect, useRef, useLayoutEffect } from "react";
import { invoke } from "@tauri-apps/api/tauri";
import "./App.css";
import {Canvas} from "@react-three/fiber";
import * as THREE from "three";
import { PresentationControls, Stage, Environment, Stats, CameraControls, OrbitControls} from "@react-three/drei";
import LoadingScreen from "./LoadingScreen";
import { Grid } from "@mui/joy";
import StatsFormat from "./gui-components/StatsFormat";
import Toolbar from "./gui-components/Toolbar";
import { Panel } from "stats.js";
import PanelWelcome from "./gui-components/PanelWelcome";
import PanelEnvironment from "./gui-components/PanelEnvironment";
import { ActivePanel } from "./ActivePanel";
import PanelSmiles from "./gui-components/PanelSmiles";
import PanelLoadFile from "./gui-components/PanelLoadFile";
import PanelMolecular from "./gui-components/PanelMolecular";
import { resolveResource } from '@tauri-apps/api/path'
import { open, save } from '@tauri-apps/api/dialog';
import { useShortcut } from "./hooks/Shortcuts";
import PanelExport from "./gui-components/PanelExport";
import ModalDatabase from "./gui-components/ModalDatabase";

function App() {
  // Rendered data states //
  const [isLoading, setLoading] = useState(false);
  const [spheres, setSpheres] = useState([]);
  const [bondPairs, setBondPairs] = useState([]);
  // Rendered data states end //
  
  // environment states //
  const [environment, setEnvironment] = useState("kloofendal_43d_clear_puresky_1k.hdr");
  const [envBlur, setEnvBlur] = useState(0.5);
  const [showBackground, setShowBackground] = useState(true);
  const [bgRotation, setBgRotation] = useState(0);
  const [bgIntensity, setBgIntensity] = useState(1);
  const [ambientLight, setAmbientLight] = useState(0.5);
  // environment states end //

  // molecule states //
  const [bondThickness, setBondThickness] = useState(2);
  const [showBonds, setShowBonds] = useState(true);
  const [applyRadii, setApplyRadii] = useState(true);
  const [radiiScale, setRadiiScale] = useState(0.02);
  const [materialRoughness, setMaterialRoughness] = useState(0.2);
  const [detail, setDetail] = useState(32);
  const [bondDistance, setBondDistance] = useState(0.5);
  const [spaceScale, setSpaceScale] = useState(1);
  const [bondColor, setBondColor] = useState("#1c1c1c");
  // molecule states end //

  // database modal states //
  const [showModal, setShowModal] = useState(false);
  // database modal states end //

  // GUI States //
  const [activePanel, setActivePanel] = useState(ActivePanel.WELCOME);
  const [savePath, setSavePath] = useState(null);
  const ref = useRef(null);
  // GUI States end //

  // Shortcut handling //
  const onKeyPress = (event) => {
    saveFe2O3File();
  };

  useShortcut(['ctrl', 's'], onKeyPress);
  // useShortcut(['ctrl', 'b'], async () => {await invoke("run_openbabel", {smiles: "CCO"})})
  // Shortcut handling end //

  // BACKEND WRAPPERS //
  async function saveFe2O3File() {
    let filePath = null;
    if (savePath === null) {
      filePath = await save({
        filters: [{
          name: 'Fe2O3',
          extensions: ['fe2o3', 'bin']
        }]
      })
      setSavePath(filePath);
      console.log(filePath);
    } else {
      filePath = savePath;
    }  

    if (filePath !== null) {
      let response = await invoke("save_fe2o3", {
        applicationState: {
          spheres: spheres,
          bond_pairs: bondPairs,
          environment: environment,
          environment_blur: envBlur,
          show_background: showBackground,
          bg_rotation: bgRotation,
          bg_intensity: bgIntensity,
          ambient_light: ambientLight,
          bond_thickness: bondThickness,
          show_bonds: showBonds,
          apply_scale: applyRadii,
          scale_factor: radiiScale,
          roughness: materialRoughness,
          detail: detail,
          space_scale: spaceScale,
          bond_color: bondColor,
          fov: 45
        },
        path: filePath
      });
      console.log(response);
    }
  }

  async function loadFe2O3File() {
    const file = await open({
      multiple: false,
      filters: [{
        name: 'Fe2O3',
        extensions: ['fe2o3', 'bin']
      }]
    });

    setSavePath(file);

    setLoading(true);

    if (file !== null) {
      let response = await invoke("load_fe2o3", {path: file});
      setSpheres(response.spheres.map((sphere) => [sphere.coords, sphere.ids, sphere.color]));
      setBondPairs(response.bond_pairs);
      setEnvironment(response.environment);
      setEnvBlur(response.environment_blur);
      setShowBackground(response.show_background);
      setBgRotation(response.bg_rotation);
      setBgIntensity(response.bg_intensity);
      setAmbientLight(response.ambient_light);
      setBondThickness(response.bond_thickness);
      setShowBonds(response.show_bonds);
      setApplyRadii(response.apply_scale);
      setRadiiScale(response.scale_factor);
      setMaterialRoughness(response.roughness);
      setDetail(response.detail);
      setSpaceScale(response.space_scale);
      setBondColor(response.bond_color);
      // setFov(response.fov);
      console.log(response);
    }

    setLoading(false);
  }

  async function loadMol2File() {
    const selected = await open({
      multiple: false,
      filters: [{
        name: 'Mol2 File',
        extensions: ['mol2', 'mol', 'txt']
      }]
    });

    setLoading(true);

    if (selected !== null) {
      let response = await invoke("create_molecule", {repr: selected, mol2Mode: true});
      let arr = [response.sphere_coords, response.sphere_ids, response.colors]; 
      let output = arr[0].map((_, colIndex) => arr.map(row => row[colIndex]))
      console.log(output);
      setSpheres(output);
      setBondPairs(response.bond_pairs);
      setLoading(false);
    }
  }

  async function loadSmiles(smiles) {
    let response = await invoke("create_molecule", {repr: smiles, mol2Mode: false});
    let arr = [response.sphere_coords, response.sphere_ids, response.colors]; 
    let output = arr[0].map((_, colIndex) => arr.map(row => row[colIndex]))
    console.log(output);
    setSpheres(output);
    setBondPairs(response.bond_pairs);
  }
  // BACKEND WRAPPERS END //

  // Other utils //

  async function saveImage(format) {
    let path = await save({
      filters: [{
        name: 'Image',
        extensions: [format]
      }]
    });
    let ctx = document.getElementById("canvasMain").children[0].children[0];
    let data = ctx.toDataURL("image/png");
    let response = await invoke("save_image", {dataUrl: data, path: path, format: format});
    console.log(response);
  }
  // Other utils end //

  // Primitives definitions //
  function Line({ start, end }) {
    const ref = useRef()
    useLayoutEffect(() => {
      ref.current.geometry.setFromPoints([start, end].map((point) => new THREE.Vector3(...point)))
    }, [start, end])
    return (
      <line ref={ref}>
        <bufferGeometry />
        <lineBasicMaterial color={bondColor} linewidth={bondThickness} clipIntersection={true} roughness={0.2} />
      </line>
    )
  }

  function Sphere({ vertices, indices, color}) {
    const geom = useRef();
  
    useEffect(() => {
      geom.current.setAttribute('position', new THREE.Float32BufferAttribute(vertices, 3));
      geom.current.setIndex(indices);
      geom.current.computeVertexNormals();
    }, [vertices, indices]);
  
    return (
      <mesh>
        <bufferGeometry ref={geom} attach="geometry" />
        <meshStandardMaterial color={color} roughness={materialRoughness} clipIntersection={true} clipShadows={true} side={THREE.BackSide}/>
      </mesh>
    );
  }

  // Primitives definitions end //

  // Main render //
  if (isLoading) {
    return <LoadingScreen />;
  } else {
    return (
      <>
      <Grid container spacing={2} sx={{ flexGrow: 1 }} style={{height: 1080, minHeight: 1080}}>
        <Grid item xs={9}>
          <Grid container spacing={2} sx={{ flexGrow: 1 }} style={{position: "absolute", top: 16, left: 16, width: "100%", zIndex: 999}}>
            <Grid item xs={12}>
              <Toolbar setActivePanel={setActivePanel} openDbModal={() => {setShowModal(true)}}/>
            </Grid>
          </Grid>
          <Canvas dpr={[1, 2]} camera={{fov: 45}} height={"1080"} position={"absolute"} gl={{preserveDrawingBuffer: true}} ref={ref} id={"canvasMain"}>
            <group>
              {spheres.map((sphere, index) => {
                return <Sphere key={index} vertices={sphere[0]} indices={sphere[1]} color={sphere[2]} />
              })}
              {/*spheres.map((sphere, index) => {
                return <Text key={index} position={sphere[0]} color={sphere[2]} fontSize={0.5} maxWidth={100} lineHeight={1} letterSpacing={0.02} textAlign={"center"} font="https://fonts.gstatic.com/s/raleway/v14/1Ptrg8zYS_SKggPNwIYqWqhPBQ.ttf">{index}</Text>
              })*/}
              {showBonds && bondPairs.map((pair, index) => { // the bonds are rendered only if the amount of molecules is lower than 100
                return <Line key={index} start={pair[0]} end={pair[1]} />
              })}
            </group>
            <Environment files={`../${environment}`} background={showBackground} backgroundBlurriness={envBlur} backgroundIntensity={bgIntensity} backgroundRotation={[0, bgRotation, 0]} />
            <ambientLight intensity={ambientLight} />
            <OrbitControls minPolarAngle={-1 * Math.PI} maxPolarAngle={Math.PI} />
          {/* <Stats /> */}
          </Canvas>
        </Grid>
        <Grid item xs={3}>
          {activePanel === ActivePanel.WELCOME && <PanelWelcome />}
          {activePanel === ActivePanel.ENVIRONMENT_OPTIONS && <PanelEnvironment setTexture={setEnvironment} setBlur={setEnvBlur} background={showBackground} toggleBackground={setShowBackground} blur={envBlur} rotation={bgRotation} setRotation={setBgRotation} intensity={bgIntensity} setIntensity={setBgIntensity} ambientLight={ambientLight} setAmbientLight={setAmbientLight}/>}
          {activePanel === ActivePanel.INPUT_SMILES && <PanelSmiles loadSmiles={loadSmiles} />}
          {activePanel === ActivePanel.LOAD_FILE && <PanelLoadFile loadMol2={loadMol2File} loadFe2O3={loadFe2O3File} />}
          {activePanel === ActivePanel.MOLECULE_OPTIONS && <PanelMolecular showBonds={showBonds} toggleBonds={setShowBonds} bondThickness={bondThickness} setBondThickness={setBondThickness} applyRadii={applyRadii} toggleRadii={setApplyRadii} radiiScale={radiiScale} setRadiiScale={setRadiiScale} detail={detail} setDetail={setDetail} materialRoughness={materialRoughness} setMaterialRoughness={setMaterialRoughness} spaceScale={spaceScale} setSpaceScale={setSpaceScale} bondDistance={bondDistance} setBondDistance={setBondDistance} bondColor={bondColor} setBondColor={setBondColor}/>}
          {activePanel === ActivePanel.EXPORT_OPTIONS && <PanelExport saveImage={saveImage} />}
        </Grid>
      </Grid>
      <Grid container spacing={2} sx={{ flexGrow: 1 }} style={{position: "absolute", bottom: 8, backgroundColor: "#333333", width: "100%"}}>
        <Grid item xs={6}>
          Release 1.0.0: Hematite
        </Grid>
        <Grid item xs={6}>
          <StatsFormat 
            molecules={spheres}
            bonds={bondPairs}
            detail={detail}
          />
        </Grid>
      </Grid>
      <ModalDatabase setOpen={setShowModal} isOpen={showModal} loadSmiles={loadSmiles}/>
      </>
    );
  }
}

export default App;
