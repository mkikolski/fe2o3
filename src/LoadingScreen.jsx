import React from 'react'
import loadingAnim from "./fe2o3_loading.json"; // adapted from https://lottiefiles.com/78xtwz1qwnrmgtt9 by @satheesh_m
import Lottie from 'lottie-react';
import { Box, Typography } from '@mui/joy';

export default function LoadingScreen(props) {
  let title = "Loading Fe<sub>2</sub>O<sub>3</sub>..."
  let desc = "This may take a while if your molecule is a bigger protein."
  if (props.title) {
    title = props.title
  }
  if (props.desc) {
    desc = props.desc
  }
  return (
    <Box textAlign={"center"} paddingTop={12}>
        <Lottie animationData={loadingAnim} loop={true} style={{height: 128, paddingBottom: 16, top: 100}}/>
        <Typography variant={"h4"} fontFamily={"JetbrainsMono"}>{title}</Typography>
        <Typography variant={"body1"} fontFamily={"JetbrainsMono"}>{desc}</Typography>
    </Box>
  )
}
