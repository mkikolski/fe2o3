import React, { useState } from 'react'
import Grid from '@mui/joy/Grid';
import Typography from '@mui/joy/Typography';
import Modal from '@mui/joy/Modal';
import ModalClose from '@mui/joy/ModalClose';
import { ModalDialog } from '@mui/joy';
import { PhotoshopPicker } from 'react-color';
import {Sheet} from '@mui/joy';

export default function ColorPicker(props) {
    const [isActive, setIsActive] = useState(false);
    const [selectedColor, setSelectedColor] = useState(props.color)

    const hex2rgb = (hex) => {
        return hex.match(/[A-Za-z0-9]{2}/g).map(v => parseInt(v, 16));
    }

    const handleChange = (color) => {
        props.setColor(color.hex)
    }

    return (
        <div>
            <Grid container spacing={2}>
                <Grid item xs={4} sx={{backgroundColor: props.color}} onClick={() => {
                    setSelectedColor(props.color)
                    setIsActive(true)
                }}>
                    
                </Grid>
                <Grid item xs={8}>
                    <Typography variant={"body1"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={18} style={{display: "inline-block", verticalAlign: "middle"}}>{props.atomName}</Typography>
                </Grid>
            </Grid>
            <Modal open={isActive} onClose={() => {
                props.setColor(selectedColor)
                setIsActive(false)}
                }>
                    <ModalDialog sx={{padding: 0}}>
                        <PhotoshopPicker onCancel={() => {
                            props.setColor(selectedColor)
                            setIsActive(false)}
                        } style={{fontFamily: "JetbrainsMono !important"}} color={props.color} onChangeComplete={
                            handleChange
                        } onChange={
                            handleChange
                        } onAccept={ (color) => {
                            setIsActive(false)
                        }     
                        }/>
                    </ModalDialog>
            </Modal>
        </div>
    )
}
