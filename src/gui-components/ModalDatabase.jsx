import React, {useState} from 'react'
import { AiOutlineSearch } from "react-icons/ai";
import { Button, Divider, Grid, Modal, ModalDialog, Textarea, DialogTitle, Typography } from '@mui/joy';
import LoadingScreen from '../LoadingScreen';
import SearchRow from './SearchRow';
import { invoke } from '@tauri-apps/api';

export default function ModalDatabase(props) {
    const [query, setQuery] = useState("");
    const [loading, setLoading] = useState(0);
    const [results, setResults] = useState([]);


    const onSearchClick = async() => {
        console.log("clicked")
        setLoading(1)
        const response = await invoke("load_compounds_from_name", {name: query});
        if (response.length === 0) {
            setLoading(4)
            return;
        }
        setResults(response.compounds)
        setLoading(2)
    }

    return (
        <React.Fragment>
            <Modal open={props.isOpen} onClose={() => {
                props.setOpen(false)
                setQuery("")
                setResults([])
                setLoading(0)
                }}>
                <ModalDialog sx={{backgroundColor: "#333333"}} layout="center" maxWidth={"80vw"} minWidth={"80vw"} >
                    <DialogTitle sx={{color: 'white', fontFamily: 'JetbrainsMono'}}>Search PubChem database</DialogTitle>
                    <Grid container spacing={2}>
                        <Grid item xs={10}>
                            <Textarea
                                placeholder='Enter compound name an press "Search"'
                                value={query}
                                onChange={(e) => setQuery(e.target.value)}
                                minRows={1}
                                maxRows={1}
                                sx={{
                                    backgroundColor: 'rgba(255, 255, 255, 0.1)',
                                    color: 'white',
                                    fontFamily: 'JetbrainsMono',
                                    fontSize: 14,
                                    padding: 2,
                                    marginTop: 2,
                                    marginBottom: 2,
                                    width: "100%",
                                    '--Textarea-focusedInset': 'var(--any, )',
                                    '--Textarea-focusedThickness': '0.25rem',
                                    '--Textarea-focusedBorderColor': '#eb4034',
                                    '--Textarea-focusedHighlight': '#eb4034',
                                    '&::before': {
                                        transition: 'box-shadow .15s ease-in-out',
                                    },
                                    '&:focus-within': {
                                        borderColor: '#eb4034',
                                    },
                                    '&:focus-within::before': {
                                        boxShadow: '0 0 0 var(--Textarea-focusedThickness) var(--Textarea-focusedBorderColor)',
                                    }
                                }}
                            />
                        </Grid>
                        <Grid item xs={2} sx={{justifyContent: "center", display: "flex", padding: 3}}>
                            <Button 
                                onClick={() => {
                                    onSearchClick()
                                }}
                                sx={{
                                    backgroundColor: '#eb4034',
                                    color: 'white',
                                    fontFamily: 'JetbrainsMono',
                                    fontSize: 14,
                                    padding: 1,
                                    width: "100%",
                                    '&:hover': {
                                        backgroundColor: '#eb4034',
                                        opacity: 0.8
                                    }
                                }}
                            >
                                <AiOutlineSearch/> Search
                            </Button>
                        </Grid>
                        <Grid item xs={12}>
                            <Divider/>
                        </Grid>
                        <Grid item xs={12} sx={{overflow: "hidden"}}>
                            <div style={{heiht: "80vh", maxHeight: "80vh", minHeight: "80vh", overflowY: "scroll", marginBottom: "32px"}}>
                                {loading === 0 && <Typography variant={"body1"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={14} textAlign={"center"}>
                                    Waiting for search query...
                                </Typography>}
                                {loading === 4 && <Typography variant={"body1"} fontFamily={"JetbrainsMono"} textColor={"red"} fontSize={14} textAlign={"center"}>
                                    No results, refine the query...
                                </Typography>}
                                {loading === 1 && <LoadingScreen title={"Fetching data..."} desc={"Your query is being processed by PubChem servers."}/>}
                                {loading === 2 && results.map((result, index) => {
                                    return <SearchRow name={result.name} url={result.image_url} cid={result.cid} key={index} smiles={result.smiles} loadSmiles={props.loadSmiles} setOpen={props.setOpen}/>
                                })}
                            </div>
                        </Grid>
                    </Grid>
                </ModalDialog>
            </Modal>
        </React.Fragment>
    )
}
