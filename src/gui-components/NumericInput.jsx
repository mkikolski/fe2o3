import React, {useRef} from 'react'
import Grid from '@mui/joy/Grid';
import Typography from '@mui/joy/Typography';
import Input from '@mui/joy/Input';

export default function NumericInput(props) {
    const np = props.numericProps
    const inputRef = useRef(null);
    return (
        <Grid container spacing={2} sx={{ flexGrow: 1 }} style={{paddingTop: 16, paddingBottom: 8}} justifyContent={"center"}>
            <Grid item xs={8}>
            <Typography variant={"body1"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={18} style={{display: "inline-block", verticalAlign: "middle"}}>{np.label}</Typography>
            </Grid>
            <Grid item xs={4}>
            <Input
                type="number"
                defaultValue={np.defaultValue}
                slotProps={{
                input: {
                    ref: inputRef,
                    min: np.min,
                    max: np.max,
                    step: np.step,
                },
                }}
                onChange={(event) => {
                    console.log(event.target.value)
                    np.setter(event.target.value)
                }}
                value={np.value}
                sx={{
                    backgroundColor: 'rgba(255, 255, 255, 0.1)',
                    color: 'white',
                    fontFamily: 'JetbrainsMono',
                    fontSize: 16,
                    padding: 2,
                    marginTop: 2,
                    marginBottom: 2,
                    width: "100%",
                    '--Input-focusedInset': 'var(--any, )',
                    '--Input-focusedThickness': '0.25rem',
                    '--Input-focusedBorderColor': '#eb4034',
                    '--Input-focusedHighlight': '#eb4034',
                    '&::before': {
                        transition: 'box-shadow .15s ease-in-out',
                    },
                    '&:focus-within': {
                        borderColor: '#eb4034',
                    },
                    '&:focus-within::before': {
                        boxShadow: '0 0 0 var(--Input-focusedThickness) var(--Input-focusedBorderColor)',
                    }
                }}
            />
            </Grid>
        </Grid>
    )
}
