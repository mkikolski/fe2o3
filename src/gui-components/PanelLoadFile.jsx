import React from 'react'
import { Button, Divider, Typography } from '@mui/joy'

export default function PanelLoadFile(props) {
    console.log(props)
  return (
    <div style={{padding: 16}}>
        <Typography variant={"h4"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={24}>Load file</Typography>
        <Divider/>
        <Button 
                onClick={() => {
                    props.loadMol2();
                }}
                sx={{
                    backgroundColor: '#eb4034',
                    color: 'white',
                    fontFamily: 'JetbrainsMono',
                    fontSize: 16,
                    padding: 2,
                    marginTop: 2,
                    marginBottom: 2,
                    width: "100%",
                    '&:hover': {
                        backgroundColor: '#eb4034',
                        opacity: 0.8
                    }
                }}
            >
                Load MOL2 file
            </Button>
            <Button 
                onClick={
                    () => {
                        props.loadFe2O3();
                    }
                }
                sx={{
                    backgroundColor: 'rgba(0,0,0,0)',
                    border: '2px solid #eb4034',
                    color: '#eb4034',
                    fontFamily: 'JetbrainsMono',
                    fontSize: 16,
                    padding: 2,
                    marginTop: 2,
                    marginBottom: 2,
                    width: "100%",
                    '&:hover': {
                        backgroundColor: 'rgba(235, 64, 52, 0.2)',
                        borderColor: '#eb4034',
                        color: "white"
                    }
                }}
            >
                Load Fe2O3 file
            </Button>
            <Typography variant={"h4"} fontFamily={"JetbrainsMono"} fontSize={14} textAlign={"justify"}>
                This program supports its very own binary file format (*.fe2o3) for storing structural information alongside with
                settings created while using this software (e.g. camera position, background settings, etc.).
                It can be a good way to save your work and progress and share it with others.
            </Typography>
    </div>
  )
}
