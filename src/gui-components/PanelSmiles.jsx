import React from 'react'
import { Button, Divider, Typography } from '@mui/joy'
import Textarea from '@mui/joy/Textarea';
import { useState } from 'react';
import Tooltip from '@mui/joy/Tooltip';

export default function PanelSmiles(props) {
    const [smiles, setSmiles] = useState("");
    const [forbiddenSymbols, setForbiddenSymbols] = useState(false);

    return (
        <div style={{padding: 16}}>
            <Typography variant={"h4"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={24}>Input SMILES string</Typography>
            <Divider/>
            <Textarea
                placeholder='Enter SMILES string'
                value={smiles}
                onChange={(e) => setSmiles(e.target.value)}
                minRows={6}
                maxRows={8}
                sx={{
                    backgroundColor: 'rgba(255, 255, 255, 0.1)',
                    color: 'white',
                    fontFamily: 'JetbrainsMono',
                    fontSize: 16,
                    padding: 2,
                    marginTop: 2,
                    marginBottom: 2,
                    width: "100%",
                    '--Textarea-focusedInset': 'var(--any, )',
                    '--Textarea-focusedThickness': '0.25rem',
                    '--Textarea-focusedBorderColor': '#eb4034',
                    '--Textarea-focusedHighlight': '#eb4034',
                    '&::before': {
                        transition: 'box-shadow .15s ease-in-out',
                    },
                    '&:focus-within': {
                        borderColor: '#eb4034',
                    },
                    '&:focus-within::before': {
                        boxShadow: '0 0 0 var(--Textarea-focusedThickness) var(--Textarea-focusedBorderColor)',
                    }
                }}
            />
            {forbiddenSymbols && <Typography variant={"body1"} fontFamily={"JetbrainsMono"} textColor={"red"} fontSize={14} >The SMILES string contains forbidden symbols!</Typography>}
            <Button 
                onClick={() => {
                    const forbidden = smiles.match(/[^A-Za-z0-9@#\(\)\[\]\.\-\+=]+/g);
                    if (forbidden) {
                        setForbiddenSymbols(true);
                        return;
                    }
                    setForbiddenSymbols(false);
                    props.loadSmiles(smiles);
                }}
                sx={{
                    backgroundColor: '#eb4034',
                    color: 'white',
                    fontFamily: 'JetbrainsMono',
                    fontSize: 16,
                    padding: 2,
                    marginTop: 2,
                    marginBottom: 2,
                    width: "100%",
                    '&:hover': {
                        backgroundColor: '#eb4034',
                        opacity: 0.8
                    }
                }}
                disabled={smiles === ""}
            >
                {(smiles === "") ? "Enter text to proceed" : "Process SMILES"}
            </Button>
        </div>
    )
}
