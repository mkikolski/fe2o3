import { Typography } from '@mui/joy';
import {React, useState} from 'react'
import Stats from 'stats.js';

export default function StatsFormat(props) {

    console.log(props.molecules[0]);
    
    return (
        <Typography variant={"body1"} fontFamily={"JetbrainsMono"} textAlign={"right"}><b>Vertices: </b>{props.molecules.length * props.detail}   <b>Atoms: </b>{props.molecules.length}   <b>Bonds: </b>{props.bonds.length}</Typography>
    )
}
