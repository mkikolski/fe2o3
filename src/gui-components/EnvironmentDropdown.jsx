import React from 'react'
import Avatar from '@mui/joy/Avatar';
import ListItemDecorator from '@mui/joy/ListItemDecorator';
import ListDivider from '@mui/joy/ListDivider';
import Select from '@mui/joy/Select';
import Option from '@mui/joy/Option';
import { Typography } from '@mui/joy';

const options = [
    { value: 'kloofendal_43d_clear_puresky_1k.hdr', label: 'Clear sky', src: "../../public/puresky_thumbnail.png"},
    { value: 'blocky_photo_studio_2k.hdr', label: 'Photo studio', src: '../../public/photostudio_thumbnail.png' },
    { value: 'kloppenheim_02_1k.hdr', label: 'Night', src: '../../public/night_thumbnail.png' },
    { value: 'studio_small_08_1k.hdr', label: 'Photo studio 2', src: '../../public/photostudio2_thumbnail.png' },
    { value: 'belfast_sunset_1k.hdr', label: 'Belfast', src: '../../public/belfast_thumbnail.png' },
    { value: 'overcast_soil_puresky_1k.hdr', label: 'Clouds', src: '../../public/clouds_thumbnail.png' },
    { value: 'metro_vijzelgracht_1k.hdr', label: 'Metro', src: '../../public/metro_thumbnail.png' },
    { value: 'rural_asphalt_road_1k.hdr', label: 'Sunny', src: '../../public/sunny_thumbnail.png' },
];

function renderValue(option) {
    if (!option) {
      return null;
    }
  
    return (
      <React.Fragment>
        <ListItemDecorator>
          <Avatar size="sm" src={options.find((o) => o.value === option.value)?.src} />
        </ListItemDecorator>
        <Typography variant="body1" fontFamily="JetbrainsMono" textColor={"white"} style={{paddingLeft: 24}}>{option.label}</Typography>
      </React.Fragment>
    );
}


export default function EnvironmentDropdown(props) {
    return (
        <Select
        defaultValue="kloofendal_43d_clear_puresky_1k.hdr"
        slotProps={{
            listbox: {
            sx: {
                '--ListItemDecorator-size': '32px',
                backgroundColor: '#333333',
                color: 'white',
                fontFamily: 'JetbrainsMono',
                borderRadius: 4,
            },
            },
        }}
        sx={{
            '--ListItemDecorator-size': '32px',
            minWidth: 192,
            backgroundColor: '#333333',
            color: 'white',
            fontFamily: 'JetbrainsMono',
            borderRadius: 4,
            '&:hover': {
            backgroundColor: '#272727',
            },
        }}
        renderValue={renderValue}
        >
        {options.map((option, index) => (
            <React.Fragment key={option.value}>
            {index !== 0 ? <ListDivider role="none" inset="startContent" /> : null}
            <Option value={option.value} label={option.label} backgroundColor={"#333333"} sx={{
                backgroundColor: '#333333',
                color: 'white',
                fontFamily: 'JetbrainsMono',
                '&:hover': {
                backgroundColor: '#272727 !important',
                },
                '&.Mui-selected': {
                    backgroundColor: '#272727 !important',
                }
            }} onClick={() => {
                props.setTexture(option.value);
            }}>
                <ListItemDecorator>
                <Avatar size="sm" src={option.src} />
                </ListItemDecorator>
                <Typography variant="body1" fontFamily="JetbrainsMono" textColor={"white"} style={{paddingLeft: 24}}>{option.label}</Typography>
            </Option>
            </React.Fragment>
        ))}
        </Select>
    )
}
