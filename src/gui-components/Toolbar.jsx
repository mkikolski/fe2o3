import {React, useState} from 'react';
import ToolButton from './ToolButton';
import { Grid } from '@mui/joy';
import { ActivePanel } from '../ActivePanel';
import { AiOutlineFolderOpen, AiOutlineEdit, AiOutlineVideoCamera, AiOutlineFileImage, AiOutlineBgColors, AiOutlineDownload, AiOutlineHome, AiOutlineExperiment, AiOutlineDatabase } from "react-icons/ai";

export default function Toolbar(props) {
    const [welcomePanelButtonState, setWelcomePanelButtonState] = useState({
        tooltip: 'Display welcome panel',
        isDisabled: false,
        isLoading: false,
        onClick: () => {
            props.setActivePanel(ActivePanel.WELCOME)
        },
        icon: (<AiOutlineHome />)
    })

    const [openFileButtonState, setOpenFileButtonState] = useState({
        tooltip: 'Open file',
        isDisabled: false,
        isLoading: false,
        onClick: () => {
            props.setActivePanel(ActivePanel.LOAD_FILE)
        },
        icon: (<AiOutlineFolderOpen />)
    })

    const [inputSmilesButtonState, setInputSmilesButtonState] = useState({
        tooltip: 'Input SMILES',
        isDisabled: false,
        isLoading: false,
        onClick: () => {
            props.setActivePanel(ActivePanel.INPUT_SMILES)
        },
        icon: (<AiOutlineEdit />)
    })

    const [moleculeOptionsButtonState, setMoleculeOptionsButtonState] = useState({
        tooltip: 'Molecule options',
        isDisabled: false,
        isLoading: false,
        onClick: () => {
            props.setActivePanel(ActivePanel.MOLECULE_OPTIONS)
        },
        icon: (<AiOutlineExperiment />)
    })

    const [environmentOptionsButtonState, setEnvironmentOptionsButtonState] = useState({
        tooltip: 'Background options',
        isDisabled: false,
        isLoading: false,
        onClick: () => {
            props.setActivePanel(ActivePanel.ENVIRONMENT_OPTIONS)
        },
        icon: (<AiOutlineFileImage />)
    })

    const [colorOptionsButtonState, setColorOptionsButtonState] = useState({
        tooltip: 'Color options',
        isDisabled: false,
        isLoading: false,
        onClick: () => {
            props.setActivePanel(ActivePanel.COLOR_OPTIONS)
        },
        icon: (<AiOutlineBgColors />)
    })

    const [databaseButtonState, setDatabaseButtonState] = useState({
        tooltip: 'Load from PubChem',
        isDisabled: false,
        isLoading: false,
        onClick: () => {
            props.openDbModal()
        },
        icon: (<AiOutlineDatabase />)
    })

    const [exportOptionsButtonState, setExportOptionsButtonState] = useState({
        tooltip: 'Export image',
        isDisabled: false,
        isLoading: false,
        onClick: () => {
            props.setActivePanel(ActivePanel.EXPORT_OPTIONS)
        },
        icon: (<AiOutlineDownload />)
    })

    return (
        <Grid container spacing={1} columns={16} sx={{ flexGrow: 1 }}>
            <Grid item xs={2}>
                <ToolButton buttonState={welcomePanelButtonState} />
            </Grid>
            <Grid item xs={2}>
                <ToolButton buttonState={inputSmilesButtonState} />
            </Grid>
            <Grid item xs={2}>
                <ToolButton buttonState={openFileButtonState} />
            </Grid>
            <Grid item xs={2}>
                <ToolButton buttonState={moleculeOptionsButtonState} />
            </Grid>
            <Grid item xs={2}>
                <ToolButton buttonState={environmentOptionsButtonState} />
            </Grid>
            <Grid item xs={2}>
                <ToolButton buttonState={colorOptionsButtonState} />
            </Grid>
            <Grid item xs={2}>
                <ToolButton buttonState={databaseButtonState} />
            </Grid>
            <Grid item xs={2}>
                <ToolButton buttonState={exportOptionsButtonState} />
            </Grid>
        </Grid>
    )
}
