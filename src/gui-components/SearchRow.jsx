import { Divider, Typography, Grid, Box } from '@mui/joy'
import React from 'react'

export default function SearchRow(props) {
    const onItemClick = async() => {
        await props.loadSmiles(props.smiles)
        props.setOpen(false)
    }

    return (
    <div>
        <Grid container spacing={2} onClick={onItemClick}>
            <Grid item xs={4}>
                <Box
                    component="img"
                    sx={{
                        height: 200,
                        width: 200,
                        maxHeight: { xs: 200, md: 200 },
                        maxWidth: { xs: 200, md: 200 },
                    }}
                    alt={props.cid}
                    src={props.url}
                />
            </Grid>
            <Grid item xs={8}>
                <Typography variant={"h4"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={16}>{props.name}</Typography>
            </Grid>
        </Grid>
        <Divider/>
    </div>
    )
}
