import React from 'react'
import { Divider, Typography, Button } from '@mui/joy'

export default function PanelExport(props) {
  return (
    <div style={{padding: 16}}>
        <Typography variant={"h4"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={24}>Export image</Typography>
        <Divider/>
        <Button 
                onClick={() => {
                    props.saveImage("png");
                }}
                sx={{
                    backgroundColor: '#eb4034',
                    color: 'white',
                    fontFamily: 'JetbrainsMono',
                    fontSize: 16,
                    padding: 2,
                    marginTop: 2,
                    marginBottom: 2,
                    width: "100%",
                    '&:hover': {
                        backgroundColor: '#eb4034',
                        opacity: 0.8
                    }
                }}
            >
                Export image
            </Button>
    </div>
  )
}
