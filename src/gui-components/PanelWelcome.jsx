import { Typography } from '@mui/joy'
import React from 'react'
import "../styles.css";

export default function PanelWelcome() {
  return (
    <div style={{padding: "16"}}>
        <Typography variant={"h4"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={28}>Welcome to Fe<sub>2</sub>O<sub>3</sub>!</Typography>
        <Typography variant={"body-xs"} fontFamily={"JetbrainsMono"} fontSize={12}>v0.6.0</Typography>
        <Typography variant={"body1"} fontFamily={"JetbrainsMono"} textColor={"white"} textAlign={"justify"}>
            Fe<sub>2</sub>O<sub>3</sub> is a molecular viewer and editor that allows to create chemical visualizations in a blink of an eye.
            Try to open a file or input a SMILES string to get started. You can also customize the camera, background, colors and other options.
            All the buttons you need are located in top-left corner of the screen.<br/><br/>
            If you want to support the project development, you can:
            <ul>
                <li>Star the project on <a href={"https://gitlab.com/mkikolski/fe2o3"} target={"_blank"} style={{
                  color: "#eb4034"
                }}>GitLab</a></li>
                <li><a href={"https://gitlab.com/mkikolski/fe2o3/-/issues/new"} target={"_blank"} style={{
                  color: "#eb4034"
                }}>Report a bug or suggest a new feature</a></li>
            </ul>
            <br/>
            Camera controls are all based on three buttons of the mouse: left for rotation, middle for zooming and right for panning.
            You can also use scroll for zooming in and out.
        </Typography>
    </div>
  )
}
