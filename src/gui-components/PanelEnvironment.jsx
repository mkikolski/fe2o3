import { Divider, Typography } from '@mui/joy'
import React from 'react'
import EnvironmentDropdown from './EnvironmentDropdown'
import Slider from '@mui/joy/Slider';
import Grid from '@mui/joy/Grid';
import AnnotatedSlider from './AnnotatedSlider';
import Checkbox from '@mui/joy/Checkbox';
import { FaCheckSquare } from "react-icons/fa";


export default function PanelEnvironment(props) {
  return (
    <div style={{padding: "16"}}>
        <Typography variant={"h4"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={24}>Background options</Typography>
        <Divider/>
        <Typography variant={"body1"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={18} style={{paddingTop: 16, paddingBottom: 8}}>Environment</Typography>
        <EnvironmentDropdown setTexture={props.setTexture}/>
        <AnnotatedSlider sliderProps={{label: "Blur", defaultValue: 0.5, step: 0.01, min: 0, max: 1, setter: props.setBlur, value: props.blur, formatter: (value) => {return value}}}/>
        <AnnotatedSlider sliderProps={{label: "Rotation", defaultValue: 0, step: 0.01, min: 0, max: 2 * Math.PI, setter: props.setRotation, value: props.rotation, formatter: (value) => {return `${(value / (2 * Math.PI) * 360).toFixed(2)}°`}}}/>
        <AnnotatedSlider sliderProps={{label: "Intensity", defaultValue: 1, step: 0.01, min: 0, max: 2, setter: props.setIntensity, value: props.intensity, formatter: (value) => {return value}}}/>
        <AnnotatedSlider sliderProps={{label: "Ambience", defaultValue: 0.5, step: 0.01, min: 0, max: 2, setter: props.setAmbientLight, value: props.ambientLight, formatter: (value) => {return value}}}/>
        <Checkbox label={"Show background"} color={"white"} checked={props.background} onChange={() => {props.toggleBackground(!props.background)}} sx={{
            color: 'white',
            fontFamily: 'JetbrainsMono',
            fontSize: 18,
            paddingTop: 4
        }} checkedIcon={
            <FaCheckSquare style={{color: "#eb4034"}}/>
        }/>
    </div>
  )
}
