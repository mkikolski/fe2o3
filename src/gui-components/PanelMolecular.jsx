import React from 'react'
import { Divider, Typography, Checkbox } from '@mui/joy'
import AnnotatedSlider from './AnnotatedSlider'
import { FaCheckSquare } from "react-icons/fa";
import ColorPicker from './ColorPicker';

export default function PanelMolecular(props) {

    return (
        <div style={{padding: 16}}>
            <Typography variant={"h4"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={24}>Molecule options</Typography>
            <Divider/>
            <Checkbox label={"Show bonds"} color={"white"} checked={props.showBonds} onChange={() => {props.toggleBonds(!props.showBonds)}} sx={{
                color: 'white',
                fontFamily: 'JetbrainsMono',
                fontSize: 18,
                paddingTop: 4
            }} checkedIcon={
                <FaCheckSquare style={{color: "#eb4034"}}/>
            }/>
            <AnnotatedSlider sliderProps={{label: "Bond thickness", defaultValue: 2, step: 0.1, min: 1, max: 24, setter: props.setBondThickness, value: props.bondThickness, formatter: (value) => {return value}}}/>
            <ColorPicker color={props.bondColor} setColor={props.setBondColor} atomName={"Bond color"}/>
            <Checkbox label={"Proportional radii"} color={"white"} checked={props.applyRadii} onChange={() => {props.toggleRadii(!props.applyRadii)}} sx={{
                color: 'white',
                fontFamily: 'JetbrainsMono',
                fontSize: 18,
                paddingTop: 4
            }} checkedIcon={
                <FaCheckSquare style={{color: "#eb4034"}}/>
            }/>
            <AnnotatedSlider sliderProps={{label: "Radii scale", defaultValue: 0.02, step: 0.01, min: 0, max: 1, setter: props.setRadiiScale, value: props.radiiScale, formatter: (value) => {return value}}}/>
            <AnnotatedSlider sliderProps={{label: "Detail", defaultValue: 32, step: 1, min: 10, max: 64, setter: props.setDetail, value: props.detail, formatter: (value) => {return value}}}/>
            <AnnotatedSlider sliderProps={{label: "Material roughness", defaultValue: 0.2, step: 0.01, min: 0, max: 1, setter: props.setMaterialRoughness, value: props.materialRoughness, formatter: (value) => {return value}}}/>
            <AnnotatedSlider sliderProps={{label: "Space scale", defaultValue: 1, step: 0.1, min: 0.5, max: 10, setter: props.setSpaceScale, value: props.spaceScale, formatter: (value) => {return value}}}/>
            <AnnotatedSlider sliderProps={{label: "Bond distance", defaultValue: 1.5, step: 0.1, min: 0.1, max: 5, setter: props.setBondDistance, value: props.bondDistance, formatter: (value) => {return value}}}/>
        </div>
    )
}
