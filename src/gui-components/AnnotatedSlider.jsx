import React from 'react'
import Slider from '@mui/joy/Slider';
import Grid from '@mui/joy/Grid';
import Typography from '@mui/joy/Typography';

export default function AnnotatedSlider(props) {
    const sl = props.sliderProps
    return (
        <Grid container spacing={2} sx={{ flexGrow: 1 }} style={{paddingTop: 16, paddingBottom: 8}} justifyContent={"center"}>
                <Grid item justifyContent={"center"} xs={3}>
                    <Typography variant={"body1"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={18} style={{display: "inline-block", verticalAlign: "middle"}}>{sl.label}</Typography>
                </Grid>
                <Grid item xs={6}>
                    <Slider
                        defaultValue={sl.defaultValue}
                        step={sl.step}
                        min={sl.min}
                        max={sl.max}
                        onChange={(e, v) => sl.setter(v)}
                        sx={{
                            width: "100%",
                            "--Slider-trackSize": "10px",
                            "--Slider-thumbBackground": "#eb4034",
                            color: "#eb4034",
                            '& .MuiSlider-rail': {
                                backgroundColor: '#333333',
                            },
                            '& .MuiSlider-thumb': {
                                backgroundColor: '#eb4034',
                                border: 'none !important',
                                boxShadow: 'none',
                                borderColor: '#eb4034',
                                outline: 'none',
                            },
                            '& .MuiSlider-thumb:before': {
                                backgroundColor: '#eb4034',
                                border: 'none !important',
                                boxShadow: 'none',
                                borderColor: '#eb4034',
                                outline: 'none',
                            },
                            '& .MuiSlider-track': {
                                backgroundColor: '#eb4034',
                            },
                        }}
                        style={{display: "inline-block", verticalAlign: "middle"}}
                    />
                </Grid>
                <Grid item xs={3}>
                    <Typography variant={"body1"} fontFamily={"JetbrainsMono"} textColor={"white"} fontSize={18} textAlign={"right"}>{sl.formatter(sl.value)}</Typography>
                </Grid>
            </Grid>
    )
}
