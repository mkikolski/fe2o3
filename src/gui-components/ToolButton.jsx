import { Box, IconButton, Tooltip } from '@mui/joy'
import React from 'react'

export default function ToolButton(props) {
  const bs = props.buttonState

  return (
    <Tooltip title={bs.tooltip} sx={{
      fontFamily: 'JetbrainsMono',
    }}>
      <IconButton disabled={bs.isDisabled} loading={bs.isLoading} onClick={bs.onClick} sx={{
        backgroundColor: '#333333 !important',
        color: '#ffffff !important',
        '&:hover': {
          backgroundColor: '#272727 !important',
        },
      }}>
        {bs.icon}
      </IconButton>
    </Tooltip>
  )
}
