import { useCallback, useEffect, useLayoutEffect, useRef } from 'react';

export const useShortcut = (keys, callback, node = null) => {
    let mods = keys.map((key) => {
        if (["ctrl", "alt", "shift"].includes(key)) {
            return key;
        }
    });
    const callbackRef = useRef(callback);
    useLayoutEffect(() => {
        callbackRef.current = callback;
    });

    const handleKeyPress = useCallback(
        (event) => {
            if (mods.includes("ctrl") && !event.ctrlKey) return;
            if (mods.includes("alt") && !event.altKey) return;
            if (mods.includes("shift") && !event.shiftKey) return;
            if (keys.some((key) => event.key === key)) {
                callbackRef.current(event);
            }
        },
        [keys]
    );

    useEffect(() => {
        // target is either the provided node or the document
        const targetNode = node ?? document;
        // attach the event listener
        targetNode &&
        targetNode.addEventListener("keydown", handleKeyPress);

        // remove the event listener
        return () =>
        targetNode &&
            targetNode.removeEventListener("keydown", handleKeyPress);
    }, [handleKeyPress, node]);
}