# Fe<sub>2</sub>O<sub>3</sub>

Aplikacja do renderowania obrazów cząsteczek związków chemicznych na podstawie ich notacji.

## Stan prac - 24.06. Wersja 1.0.0: Hematite.

Aplikacja wspiera:
- wczytywanie cząsteczki z plików `*.mol2` oraz w notacji SMILES
- własny format pliku binarnego pozwalający zapisać ustawienia renderowanej sceny
- wczytywanie danych strukturalnych z bazy danych [PubChem](https://pubchem.ncbi.nlm.nih.gov/)
- renderowanie sceny do formatu `PNG`
- edycję parametrów renderowania
- wygodną kontrolę kamery

Pozbyto się wszystkich ścieżek absolutnych, które powodowały błędy w aplikacji.
Obecnie brak wsparcia dla skalowania cząsteczek oraz zmiany koloru cząsteczek, ich wydanie zostało odłożone na dalsze wersje.

[Latest release](https://gitlab.com/mkikolski/fe2o3/-/releases/v1.0.0-hotfix)
[Prezentacja działania](https://www.youtube.com/watch?v=hUpWq2jdQco)

## TODO

- stworzenie testów
- bardziej zaawansowana obsługa błędów
- pozbycie się dead code'u oraz enkapsulacja danch

## Uruchomienie

Możliwe jest pobranie [najnowszego wydania aplikacji](https://gitlab.com/mkikolski/fe2o3/-/releases/v1.0.0-hotfix) i instalacja za pomocą komendy:
```shell
sudo dpkg -i fe2o3_1.0.0_amd64.deb
```
lub zbudowanie aplikacji samodzielnie. Proponowane jest wykorzystanie do tego `npm` lub `cargo`. W tym przypadku konieczne są:

### Zainstalowanie zależności
```shell
sudo apt update
sudo apt install libwebkit2gtk-4.0-dev \
    build-essential \
    curl \
    wget \
    file \
    libssl-dev \
    libgtk-3-dev \
    libayatana-appindicator3-dev \
    librsvg2-dev
```

### Zainstalowanie kompilatora języka Rust
```shell
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```

### Instalacja Node.js
Konieczna ze względu na wykorzystanie frameworku React do tworzenia aplikacji. Warto pobrać werjsję LTS ze strony [dewelopera](https://nodejs.org/).
Poprawność instalacji można sprawdzić wywołując polecenie:
```shell
node -v 
# np. 20.10.0
npm -v
# np. 10.2.3
```

### Instalacja `tauri-cli`
```shell
cargo install tauri-cli
```

### Budowanie projektu i uruchomienie aplikacji
```shell
# Budowa projektu i uruchomienie wersji deweloperskiej
cargo tauri dev

# Budowa plików projektu do pakietów *.deb i *.AppImage
cargo tauri build
```

Opracowano na podstawie [oficjalnej strony frameworka Tauri](https://tauri.app/v1/guides/getting-started/prerequisites).

## Wykorzystane technologie

- Rust
- Node.js
- Tauri
- React
- Vite.js
- Three.js
